package chb.web.pay.servlet;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import chb.object.resp.BaseResp;
import chb.web.pay.utils.PaymentUtil;


//该Servlet处理支付请求并进行重定向
@RestController
public class PayServlet {
	/**
	 * Servlet 生命周期
    Servlet 服务器(容器)负责管理Servlet的生命周期
              三个阶段涉及的方法
    Init()
    service()   [doPost(),doGet()]
    destroy()
    service阶段使用请求/响应方式进行通信
    
    */
	@RequestMapping(value="api/order/pay")
	public BaseResp doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("-----------");
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		String 	p0_Cmd="Buy",
				p1_MerId="10001126856",
				p2_Order=request.getParameter("userOrdersId"),
				p3_Amt="0.01",
				p4_Cur="CNY",
				p5_Pid="4454",
				p6_Pcat="Iamdd",
				p7_Pdesc="eefdf",
				p8_Url="http://erictt.iok.la:27819/api/order/back",
				p9_SAF="0",
				pa_MP="efef",
				pd_FrpId="CCB-NET-B2C",
				pr_NeedResponse="1";
		String keyValue="69cl522AV6q613Ii4W6u8K6XuW8vM1N6bFgyv769220IuYe9u37N4y7rI4Pl";
		String hmac=PaymentUtil.buildHmac(p0_Cmd, p1_MerId, p2_Order, p3_Amt, p4_Cur, p5_Pid, p6_Pcat, p7_Pdesc, p8_Url, p9_SAF, pa_MP, pd_FrpId, pr_NeedResponse, keyValue);
		String url="https://www.yeepay.com/app-merchant-proxy/node?"+
		"&p0_Cmd="+p0_Cmd+
		"&p1_MerId="+p1_MerId+
		"&p2_Order="+p2_Order+
		"&p3_Amt="+p3_Amt+
		"&p4_Cur="+p4_Cur+
		"&p5_Pid="+p5_Pid+
		"&p6_Pcat="+p6_Pcat+
		"&p7_Pdesc="+p7_Pdesc+
		"&p8_Url="+p8_Url+
		"&p9_SAF="+p9_SAF+
		"&pa_MP="+pa_MP+
		"&pd_FrpId="+pd_FrpId+
		"&pr_NeedResponse="+pr_NeedResponse+
		"&hmac="+hmac;
		//重新发送请求
		return BaseResp.ok(url);
	}
}