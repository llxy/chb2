package chb.web.pay.servlet;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.dubbo.config.annotation.Reference;

import chb.object.req.order.PayBack;
import chb.user.service.IUserOrderService;

@Controller
public class BackServlet {
	
	@Reference
	IUserOrderService userOrderService;
	
	//post请求
	@RequestMapping(value="api/order/back")
	public void doPost(PayBack payBack, HttpServletResponse resp)
			throws ServletException, IOException {
		
		String r1_Code=payBack.getR1_Code();
		if("1".equals(r1_Code)) {
			Integer back = userOrderService.dealPay(payBack);
			resp.setContentType("text/html;charset=utf-8"); 
			if(back == 0) {
				//回调处理失败
				resp.getWriter().write("error!");
			}
			resp.getWriter().write("success!");
			resp.getWriter().println("支付成功！<br/>"
					+ "商户编号："+payBack.getP1_MerId()+"<br/>"
					+ "支付金额："+payBack.getR3_Amt()+"<br/>"
					+"商户订单号："+payBack.getR6_Order()+"<br/>"
					+"支付成功时间："+payBack.getRp_PayDate());
		}
		else {
			resp.getWriter().write("error!");
		}
	}

}