package chb.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import chb.img.util.ImageUtil;
import chb.object.resp.BaseResp;
import chb.object.resp.Upload;

@RestController
@RequestMapping("api/image")
public class ImageController {
    
    @Autowired
    private ImageUtil imageUtil;
    
    @RequestMapping(value = "/shop")
    public BaseResp uploadShopImage(MultipartFile image) {
        Upload upload = new Upload();
        upload.setImageUrl(imageUtil.saveShop(image));
        return BaseResp.ok(upload);
    }
    
    @RequestMapping(value = "/user")
    public BaseResp uploadUserImage(MultipartFile image) {
        return BaseResp.ok(imageUtil.saveUser(image));
    }
    
}
