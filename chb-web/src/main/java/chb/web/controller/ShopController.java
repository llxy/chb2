package chb.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.dubbo.config.annotation.Reference;

import chb.entity.shop.Shop;
import chb.entity.shop.ShopDetail;
import chb.img.util.ImageUtil;
import chb.object.req.shop.AddDishReq;
import chb.object.req.shop.FindShopByRangeReq;
import chb.object.req.shop.ModifDishReq;
import chb.object.resp.BaseResp;
import chb.shop.service.IDishService;
import chb.shop.service.IShopService;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("api/shop")
@Slf4j
public class ShopController {
    
    @Reference
    private IShopService shopService;
    
    @Reference
    private IDishService dishService;
    
    @Autowired
    private ImageUtil imageUtil;
    
    @RequestMapping(value = "/shopMsg")
    public BaseResp findShopMsg(Integer shopId) {
        return BaseResp.ok(shopService.findShopMsg(shopId));
    }
    
    @RequestMapping(value = "/dish")
    public BaseResp findDishAndComment(Integer shopId, Integer dishId) {
        return BaseResp.ok(dishService.findDishAndComment(shopId, dishId));
    }
    
    @RequestMapping(value = "/addDish")
    public BaseResp addDish(AddDishReq req) {
        return BaseResp.ok(dishService.addDish(req).intValue());
    }
    
    @RequestMapping(value = "/getAllDish")
    public BaseResp findDish(Integer shopId, Integer dishType) {
        return BaseResp.ok(dishService.findDish(shopId, dishType));
    }
    
    @RequestMapping(value = "/delDish")
    public BaseResp delDish(Integer shopId, Integer dishId) {
        dishService.delDish(shopId, dishId);
        return BaseResp.ok();
    }
    
    @RequestMapping(value = "/modifDish")
    public BaseResp findById(ModifDishReq req) {
        dishService.modifDish(req);
        return BaseResp.ok();
    }
    
    @RequestMapping(value = "/findById")
    public BaseResp findById(Integer shopId) {
        return BaseResp.ok(shopService.findById(shopId));
    }
    
    @RequestMapping(value = "/findByRange")
    public BaseResp findAll(FindShopByRangeReq req) {
        return BaseResp.ok(shopService.findByRange(req));
    }
    
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public BaseResp add(Shop shop) {
        
        shopService.add(shop);
        return BaseResp.ok();
    }
    
    @RequestMapping(value = "/updateShop", method = RequestMethod.POST)
    public BaseResp update(ShopDetail shopDetail) {
        log.info("{}", shopDetail);
        shopService.update(shopDetail);
        return BaseResp.ok();
    }
    
    // 保存图片并返回路径
    private String addUtil(MultipartFile img) {
        if(img != null) {
            return imageUtil.saveShop(img);
        }
        return null;
    }
    
}
