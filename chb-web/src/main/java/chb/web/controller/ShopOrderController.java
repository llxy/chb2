package chb.web.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.dubbo.config.annotation.Reference;

import chb.object.resp.BaseResp;
import chb.shop.service.IShopOrderService;

@RestController
@RequestMapping("api/shopOrder")
public class ShopOrderController {
    
    @Reference
    private IShopOrderService shopOrderService;
    
    @RequestMapping(value = "")
    public BaseResp shopOrder(Integer shopId, Integer pageNum, Integer type) {
        return BaseResp.ok(shopOrderService.findOrder(shopId, type, pageNum));
    }
    
    @RequestMapping(value = "/getNewOrder")
    public BaseResp getNewOrderNum(Integer shopId) {
        return BaseResp.ok(shopOrderService.getNewOrderNum(shopId).intValue());
    }
    
    @RequestMapping(value = "/handle")
    public BaseResp getNewOrderNum(Integer shopId, Integer shopOrderId, Integer type) {
        shopOrderService.handleOrder(shopId, shopOrderId, type);
        return BaseResp.ok();
    }
    
}
