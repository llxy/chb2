/**
 * 
 */
package chb.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.dubbo.config.annotation.Reference;

import chb.entity.Orders;
import chb.entity.user.UserOrdersEntity;
import chb.object.req.order.FindOrderReq;
import chb.object.resp.BaseResp;
import chb.user.service.IUserOrderService;
import chb.web.pay.servlet.PayServlet;
import lombok.extern.slf4j.Slf4j;

/**
 * 用户订单
 * @author Eric
 */
@Slf4j
@RestController
public class OrderController {

	@Reference
	IUserOrderService userOrderService;
	
	@Autowired
	PayServlet payServlet;
	
	/** 查看订单接口 */
	@RequestMapping(value="api/order/find")
	public BaseResp findOrder(FindOrderReq findOrderReq) {
		Orders orders = userOrderService.findOrder(findOrderReq.getUserId(), 
				findOrderReq.getPage(), findOrderReq.getOrderType());
		return BaseResp.ok(orders);
	}
	
	/** 新增订单 */
	@RequestMapping(value="api/order/new")
	public BaseResp newOrder(Model model, UserOrdersEntity userOrdersEntity) {
		Integer userOrdersId = userOrderService.newOrder(userOrdersEntity);
		log.debug("newOrder id is : {}", userOrdersId);
		return BaseResp.ok(1,userOrdersId);
	}
	
	@RequestMapping(value="api/order/delete")
	public BaseResp delete(Integer userOrdersId) {
		if(userOrderService.delete(userOrdersId)!=0) {
			return BaseResp.ok();
		}
		return BaseResp.error();
	}
	
	/** 获取商户联系方式 */
	@RequestMapping(value="api/order/getPhone", method = RequestMethod.POST)
	public BaseResp getPhoneByShopId(Integer shopId){
		
		String phone = userOrderService.getPhone(shopId);
		Phone p = new Phone();
		p.setShop_phone(phone);
		return BaseResp.ok(p);
	}
	
	/** 查看订单是否支付 */
	@RequestMapping(value="api/order/handleIsPay")
	public BaseResp checkOrderState(Integer userOrdersId) {
		
		return BaseResp.ok(userOrderService.checkStatus(userOrdersId));
	}
	
	/** 支付订单
	@RequestMapping(value="api/order/pay")
	public void payOrder(HttpServletRequest req,HttpServletResponse resp) {
		
		try {
			payServlet.doGet(req, resp,"0.01");
		} catch (ServletException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	 *  */
	
	/** 用户支付平台回调
	@RequestMapping(value="api/order/back")
	public ModelAndView payBack(PayBack payBack) {
		ModelAndView model = null;
		if(userOrderService.dealPay(payBack) != 0) {
			model = new ModelAndView("index");
		} else {
			model = new ModelAndView("error");
		}
		return model;
	}
	 *  */
	
	/** 查看订单是否已支付 */
	@RequestMapping(value="api/order/check")
	public BaseResp checkPay(Integer userOrdersId) {
		if(userOrderService.checkStatus(userOrdersId) == 3) {
			return BaseResp.ok();
		}
		return BaseResp.error();
	}
	
	/** 完成订单接口 */
	@RequestMapping(value="api/order/finish")
	public BaseResp finish(Integer userOrderId) {
		if(userOrderService.findOrders(userOrderId) != 0) {
			return BaseResp.ok();
		}
		return BaseResp.error();
	}
	
//	public static void main(String[] args) {
//		System.out.println(UUID.randomUUID().toString().replace("-", "").substring(0,20));
//	}
}
