package chb.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.dubbo.config.annotation.Reference;

import chb.entity.shop.Shop;
import chb.entity.user.ShopsApplyEntity;
import chb.entity.user.UserDetailEntity;
import chb.img.util.ImageUtil;
import chb.object.code.ErrorCode;
import chb.object.req.user.ApplyReq;
import chb.object.req.user.LoginReq;
import chb.object.req.user.RegistReq;
import chb.object.req.user.ResetReq;
import chb.object.resp.BaseResp;
import chb.object.resp.Upload;
import chb.object.resp.user.Code;
import chb.object.resp.user.UserInfo;
import chb.shop.service.IShopService;
import chb.user.service.IUserDetailService;
import chb.user.service.IUserService;
import chb.web.config.LoginAuth;
import util.SaveImgPath;


@RestController
public class UserController {
    
    @Reference
    private IUserService userService;
    
    @Reference
    IUserDetailService userDetailService;
    
    @Reference
    IShopService shopService;
    
    @Autowired
    LoginAuth loginAuth;
    
    @Autowired
    ImageUtil imageUtil;
    
    @RequestMapping(value = "/api/findAllUser")
    public BaseResp findAll() {
        return BaseResp.ok(userService.findAll(new Shop()));
    }
    
    /** 登录接口 */
    @RequestMapping(value = "/api/login", method = RequestMethod.POST)
    public BaseResp login(LoginReq loginReq) {

    	try {
    		UserDetailEntity ude = userService.login(loginReq);
    		if(ude == null) {
    			return BaseResp.codeError(ErrorCode.AUTH_ERROR);
    		}
    	
    		loginAuth.setAuthMap(ude.getUserId());
    	
	    	UserInfo ui = UserInfo.builder()
	    			.userId(ude.getUserId())
	    			.shopId(ude.getShopId())
	    			.userName(ude.getUserName())
	    	    	.phone(ude.getPhone())
	    	    	.avator(ude.getAvator())
	    	    	.gender(ude.getGender())
	    	    	.acceptAddress(ude.getAcceptAddress())
	    	    	.introduction(ude.getIntroduction())
	    	    	.build();
	    	return BaseResp.ok(ui);

    	} catch (IllegalArgumentException e) {
    		return BaseResp.codeError(ErrorCode.NON_SHOPPER);
    	}
    }
    
    /** 注册接口 */
    @RequestMapping(value = "/api/regist", method = RequestMethod.POST)
    public BaseResp regist(RegistReq registReq) {
    	System.out.println(registReq.getPassword());
    	Integer vc = userService.verifyCode(registReq.getCheckCode());
    	/** 错误分支 */
    	if(vc != 0) {
    		return BaseResp.codeError(vc);
    	} else {
    		userService.regist(registReq);
    	}
    	return BaseResp.ok();
    }
    
    /** 获取验证码接口 */
    @RequestMapping(value = "/api/code", method = RequestMethod.POST)
    public BaseResp code(String phone) {
    	System.out.println(phone);
    	Code code = userService.getVerifyCode(phone);
    	if(code == null) {
    		return BaseResp.error();
    	}
    	return BaseResp.ok();
    }
    
    /** 重置密码接口 */
    @RequestMapping(value = "/api/reset", method = RequestMethod.POST)
    public BaseResp reset(ResetReq resetReq) {
    	Integer vc = userService.verifyCode(resetReq.getCheckCode());
    	if(vc != 0) {
    		return BaseResp.codeError(vc);
    	}
    	Integer code = userService.resetPwd(resetReq);
    	
    	return BaseResp.ok(code);
    }
    
    /** 获取个人信息接口 */
    @RequestMapping(value = "/api/info/check", method = RequestMethod.POST)
    public BaseResp infoCheck(@RequestParam(value="userId")Integer userId) {
    	UserDetailEntity ude = userDetailService.getUserInfo(userId);
    	return BaseResp.ok(ude);
    }
    
    /** 修改个人信息接口 */
    @RequestMapping(value = "/api/info/modify", method = RequestMethod.POST)
    public BaseResp infoModify(UserDetailEntity userDetailEntity) {
    	Integer modify = userDetailService.modifyInfo(userDetailEntity);
    	Assert.isTrue(modify != 0, "个人信息更新失败！");
    	return BaseResp.ok();
    }
    
    /** 我要开店接口 */
    @RequestMapping(value="api/info/want2OpenShop", method=RequestMethod.POST)
    public BaseResp openShop(Integer userId) {
    	
    	ShopsApplyEntity shopsApplyEntity = userService.want2OpenShop(userId);
    	
    	if(shopsApplyEntity == null) {
    		return BaseResp.error();
    	}
    	return BaseResp.ok(shopsApplyEntity);
    }
    
    /** 申请成为商家接口 */
    @RequestMapping(value="/api/info/apply", method=RequestMethod.POST)
    public BaseResp apply(ApplyReq applyReq) {
    	
    	ShopsApplyEntity sae = SaveImgPath.turnToEntity(applyReq);
    	Assert.isTrue(userService.apply2bshopper(sae) != 0, "上传失败！");
    	return BaseResp.ok();
    }
    /** 获取申请状态接口 */
    @RequestMapping(value="/api/info/applystate", method=RequestMethod.POST)
    public BaseResp applystate(Integer userId) {
    	return BaseResp.ok(userService.applyState(userId));
    }
    
    /** 图片上传接口 */
    @RequestMapping(value="/api/upload")
    public BaseResp uploadImage(MultipartFile image) {
    	String imageUrl = imageUtil.saveUser(image);
    	Upload upload = new Upload();
    	upload.setImageUrl(imageUrl);
    	return BaseResp.ok(upload);
    } 
    
    /** 登出接口 */
    @RequestMapping(value="/api/info/logout", method=RequestMethod.POST)
    public BaseResp logout(Integer userId) {
    	loginAuth.getAuthMap().remove(userId);
    	return BaseResp.ok();
    }
    
    /** 获取商铺类型接口 */
    @RequestMapping(value="/api/types")
    public BaseResp getShopTypes() {
    	return BaseResp.ok(userService.getShopTypes());
    }
    
    /** 获取菜品类型接口 */
    @RequestMapping(value="/api/foodType/management/all")
    public BaseResp getFoodTypes() {
        return BaseResp.ok(shopService.findAllFoodType());
    }
    
    @RequestMapping(value="/api/test")
    public ModelAndView test() {
    	ModelAndView model = new ModelAndView("index"); 
    	return model;
    }
    
}
