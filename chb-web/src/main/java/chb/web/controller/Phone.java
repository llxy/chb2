/**
 * 
 */
package chb.web.controller;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Eric
 */
@Getter
@Setter
public class Phone {

	private String shop_phone;
}
