package chb.web.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.dubbo.config.annotation.Reference;

import chb.entity.shop.DishComment;
import chb.object.resp.BaseResp;
import chb.shop.service.IDishService;

@RestController
@RequestMapping("api/comment")
public class CommentController {
    
    @Reference
    private IDishService dishService;
    
    @RequestMapping("/dish")
    public BaseResp findDishComment(Integer shopId, Integer pageNum) {
        return BaseResp.ok(dishService.findComment(shopId, pageNum));
    }
    
    @RequestMapping("/user")
    public BaseResp userComment(DishComment comment) {
        dishService.addComment(comment);
        return BaseResp.ok();
    }
    
}
