/**
 * 
 */
package chb.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import chb.web.config.LoginAuth;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Eric
 */
@Slf4j
public class LoginInterceptor implements HandlerInterceptor {

    @Autowired
    LoginAuth loginAuth;
    
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        
        /** 用户登录验证和状态记录 */
    	String uu = request.getParameter("userId");
        Integer userId = Integer.parseInt(request.getParameter("userId"));
        log.debug("authMap : {}", loginAuth.getAuthMap());

        if(loginAuth.getAuthMap().containsKey(userId)) {
            return true;
        }
        
        return false;
    }
    
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
            ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
    }

}
