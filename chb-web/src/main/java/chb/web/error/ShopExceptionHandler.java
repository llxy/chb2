package chb.web.error;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import chb.object.resp.BaseResp;
import chb.shop.exception.ShopException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice
public class ShopExceptionHandler {
    
    @ExceptionHandler(ShopException.class)
    @ResponseBody
    public BaseResp shopExceptionHandler(ShopException e) {
        log.info("shopExceptionHandler {}", e.getErrorCode());
        log.info("shopExceptionHandler {}", e.getMsg());
        return BaseResp.codeError(e.getErrorCode());
    }
    
    /*@ExceptionHandler(NoHandlerFoundException.class)
    public BaseResp totalExceptionHandler(NoHandlerFoundException e) {
        return BaseResp.error();
    }*/
    
    /*@ExceptionHandler(Exception.class)
    public BaseResp totalExceptionHandler(Exception e) {
        log.info("totalExceptionHandler {}", e.getMessage());
        return BaseResp.error();
    }*/
    
    /*public static final String DEFAULT_ERROR_VIEW = "index";  
    
    @ExceptionHandler(value = NoHandlerFoundException.class)  
    public ModelAndView defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {  
        ModelAndView mav = new ModelAndView();  
        mav.addObject("exception", e);
        mav.addObject("url", req.getRequestURL());  
        mav.setViewName(DEFAULT_ERROR_VIEW);  
        
        System.out.println("111111111111111111");
        return mav;  
    }*/ 
    
}
