/**
 * 
 */
package chb.web.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

/**
 *
 * @author Eric
 */
@Component
public class LoginAuth {

	Map<Integer, Long> authMap = new HashMap<>();
	
	public void setAuthMap(Integer userId) {
		long currentTime = System.currentTimeMillis();
		authMap.put(userId, currentTime);
	}
	
	public Map<Integer, Long> getAuthMap() {
		return authMap;
	}

}
