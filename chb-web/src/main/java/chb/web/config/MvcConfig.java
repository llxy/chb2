/**
 * 
 */
package chb.web.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import chb.web.interceptor.LoginInterceptor;

/**
 *
 * @author Eric
 */
@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter {
	
	
	/** 拦截器的初始化在springContext之前  
	 * 所以将拦截器用@Bean注解可以解决在拦
	 * 截器中注入bean时空指针问题 */
	@Bean
	public HandlerInterceptor getInterceptor() {
		return new LoginInterceptor();
	}
	
	@Bean
    public InternalResourceViewResolver resourceViewResolver()
    {
        InternalResourceViewResolver internalResourceViewResolver 
        		= new InternalResourceViewResolver();
        //请求视图文件的前缀地址
        internalResourceViewResolver.setPrefix("/");
        //请求视图文件的后缀
        internalResourceViewResolver.setSuffix(".html");
        return internalResourceViewResolver;
    }

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(getInterceptor())
			.addPathPatterns("/api/info/*");
		super.addInterceptors(registry);
	}
	
}
