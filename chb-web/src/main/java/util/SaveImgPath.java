/**
 * 
 */
package util;

import chb.entity.user.ShopsApplyEntity;
import chb.object.req.user.ApplyReq;

/**
 *
 * @author Eric
 */
public class SaveImgPath {

	public static ShopsApplyEntity turnToEntity(ApplyReq applyReq) {
		
		return ShopsApplyEntity.builder()
				.userId(applyReq.getUserId())
				.shopName(applyReq.getShopName())
				.shopLocation(applyReq.getShopLocation())
				.shopAbstract(applyReq.getShopAbstract())
				.identificationNum(applyReq.getIdentificationNum())
				.shopLogo(applyReq.getShopLogo())
				.identificationPic(applyReq.getIdentificationPic())
				.shopAuthImages(applyReq.getShopAuthImages())
				.shopTypeCode(applyReq.getShopTypeCode())
				.shopLongitude(applyReq.getShopLongitude())
				.shopLatitude(applyReq.getShopLatitude())
				.build();
	}
}
