package chb.web.shop;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.io.FileInputStream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.MultipartFile;

import chb.Application;
import chb.img.util.ImageUtil;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=Application.class)
public class StoreConstrollerTest {

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Before
    public void before() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void testFindAll() throws Exception {
        mockMvc.perform(post("/findAllStore")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data[0].storename").value("海大面包店"));
    }

    @Test
    public void testAdd() throws Exception {
    	
    	
         mockMvc.perform(post("/addStore")
                .param("storename", "第一饭堂"));
    }
    
    public static void main(String[] args) throws Exception {
    	
    	File f = 
    			new File("G:/sts/workspace/chb2/chb2/chb-static/src/main/resources/static/img/user/a776b759bb5a443dba24b03b2d084532.jpg");
    	FileInputStream fis = 
    			new FileInputStream(f);
    	System.out.println(fis);
        MultipartFile mfile = 
        		new MockMultipartFile("test.jpg", "a776b759bb5a443dba24b03b2d084532.jpg", "image/jpeg", fis);
        
        ImageUtil iu = new ImageUtil();
        System.out.println(iu.saveUser(mfile));
	}

}
