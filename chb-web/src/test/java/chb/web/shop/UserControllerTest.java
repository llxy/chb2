/**
 * 
 */
package chb.web.shop;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import chb.Application;

/**
 *
 * @author Eric
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes=Application.class)
public class UserControllerTest {

	@Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Before
    public void before() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }
	
	@Test
	public void testApply() throws Exception {
		mockMvc.perform(post("/api/info/apply")
                .param("storename", "第一饭堂"));
	}
}
