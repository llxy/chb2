/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.18 : Database - db_chb
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_chb` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `db_chb`;

/*Table structure for table `t_pay` */

DROP TABLE IF EXISTS `t_pay`;

CREATE TABLE `t_pay` (
  `pay_id` int(11) NOT NULL AUTO_INCREMENT,
  `r0_Cmd` varchar(20) NOT NULL COMMENT '业务类型，固定值为 buy',
  `r1_Code` varchar(255) NOT NULL COMMENT '支付结果  1代表成功',
  `r2_TrxId` varchar(50) NOT NULL COMMENT '交易流水号',
  `r3_Amt` varchar(20) NOT NULL COMMENT '支付金额 单为元',
  `r4_Cur` varchar(10) NOT NULL COMMENT '交易币种  RMB',
  `r5_Pid` varchar(20) NOT NULL COMMENT '商品名称',
  `r6_Order` varchar(50) NOT NULL COMMENT '商户定义的订单号',
  `r9_BType` varchar(1) NOT NULL COMMENT '交易结果返回类型   1为重定向通知  2服务器点对点通知',
  `rb_BankId` varchar(255) DEFAULT NULL COMMENT '银行编号',
  `ro_BankOrderId` varchar(255) DEFAULT NULL COMMENT '银行订单号',
  `rp_PayDate` varchar(255) DEFAULT NULL COMMENT '支付成功时间',
  `ru_Trxtime` varchar(255) DEFAULT NULL COMMENT '交易结果通知时间',
  PRIMARY KEY (`pay_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
