/**
 * 
 */
package chb.entity;

import java.io.Serializable;
import java.util.List;

import chb.entity.user.UserOrdersEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Eric
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Orders implements Serializable {

	/**  */
	private static final long serialVersionUID = 1L;
	
	private Integer page;
	private Integer total;
	private List<UserOrdersEntity> orders;
}
