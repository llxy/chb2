package chb.entity.shop;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class DishBriefly implements Serializable {   // 用户用户点击了某个菜品之后的返回
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
        
    private Integer dishId;      
    private String dishName;    
    private String dishImage;   
    private Double dishPrice;   
    private String dishAbstract;
    
}
