package chb.entity.shop;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Shop implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    
    private Integer shopId;
    private Integer userId;
    private String shopName;
    private Integer shopType;
    private String logo;
    private String shopAbstract;
    private String shopLocation;
    private String shopAnnouncement; 
    private Integer shopStatus;
    private String shopPhone;
    private String shopWorkTime;
    private Double shopDeliveryCost;
    private Double shopStartDelivery;
    private String storesImages;
    private String detailImages;
    private Double shopLongitude;
    private Double shopLatitude;
    private Integer cityCode;

}
