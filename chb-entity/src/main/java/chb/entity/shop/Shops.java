/**
 * 
 */
package chb.entity.shop;

import java.io.Serializable;

import lombok.Data;

/**
 * 商铺信息实体
 * @author Eric
 */
@Data
@Deprecated
public class Shops implements Serializable {

	/**  */
	private static final long serialVersionUID = 1L;
	
	private Integer shopId;           //商铺编号
	private Integer userId;           //店主id
	private String shopName;          //店铺名
	private String shopLogo;          //logo
	private String shopAbstract;      //商铺简介
	private String shopLocation;      //商铺地址
	private String shopAnnouncement;  //商铺公告
	private Integer shopStatus;       //商品状态
	private String shopPhone;         //商铺电话
	private Long shopWorkTime;        //营业时间
	private double ShopDeliveryCost;  //配送费
	private double shopStartDelivery; //起送费
	private String shopStoresImages;  //门店照
	private String shopDetailImages;  //简介照
	private double shopLongitude;     //经度
	private double shopLatitude;      //纬度
	private Integer cityCode;         //城市代码

}
