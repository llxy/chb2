package chb.entity.shop;

import java.io.Serializable;
import java.util.List;

import chb.entity.gm.FoodType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

// 为了匹配接口
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class FoodTypeList implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private List<FoodType> list;
    
}
