package chb.entity.shop;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class ShopOrder implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private Integer shopOrderId;
    private Integer userOrderId;
    private Integer userId;
    private Integer shopId;
    private String userNmae;
    private String userPhone;
    private Double orderAmount;
    private String dishIdList;
    private String dishNumList;
    private String acceptAddress;
    private String orderRemarks;
    private String time;
    private Integer orderStatus;
}
