package chb.entity.shop;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter 
@Builder
@ToString
public class ShopBriefly implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private Integer shopId;
    private String shopName;
    private String shopLogo;
    private String shopAbstract;
    private Double shopDeliveryCost;
    private Double level;
    private Integer monthlySales;

}
