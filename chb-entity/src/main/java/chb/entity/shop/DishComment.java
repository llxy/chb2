package chb.entity.shop;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class DishComment implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Integer commentId;
    private Integer orderId;
    private Integer dishId;
    private Integer shopId;
    private Integer userId;
    private String userName;
    private Float level;
    private String comment;
    private String commentDate;
    
}
