package chb.entity.shop;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Dish implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private Integer dishId;
    private Integer shopId;
    private String dishName;
    private Double dishPrice;
    private String dishImage;
    private String dishAbstract;
    private Integer dishType;
    private Float level;
    private Integer monthlySales;
    
}
