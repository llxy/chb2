package chb.entity.shop;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class ShopDetail implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private Integer shopId;
    private String shopName;
    private String logo;
    private String shopAbstract;
    private String shopLocation;
    private String shopAnnouncement; 
    private String shopPhone;
    private String shopWorkTime;
    private Double shopDeliveryCost;
    private Double shopStartDelivery;
    private String storesImages;
    private String detailImages;
    private Float level;
    private String deliveryTime;
    private Integer shopStatus;
    
    /**
     * 后端返回某些null给前端，前端修改后会传了 "null"过来。。
     * @param shopDetail
     * @return
     */
    public static ShopDetail removeNullStr(ShopDetail shopDetail) {
        if(isNull(shopDetail.getShopLocation())) shopDetail.setShopLocation("");
        if(isNull(shopDetail.getShopWorkTime())) shopDetail.setShopWorkTime("");
        return shopDetail;
    }
    
    private static boolean isNull(String str) {
        return "null".equals(str);
    }
    
}
