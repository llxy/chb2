package chb.entity.gm;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FoodType implements Serializable {

    private Integer foodTypeId;  //食物类型id
    private String typeDes; //类型描述
    private Timestamp createTime;  //创建时间
}
