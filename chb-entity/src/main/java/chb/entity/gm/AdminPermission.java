package chb.entity.gm;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class AdminPermission implements Serializable {

    private String adminName;  //管理员名字
    private Integer shopPm;     //商铺权限
    private Integer shopTypePm; //商铺类型权限
    private Integer foodTypePm; //食物类型权限
}
