package chb.entity.gm;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ShopApplyRough  implements Serializable {

    private Integer shopApplyId;  //商铺申请表id
    private Integer userId;  //用户id
    private String identificationNum;  //用户身份证号码
    private String shopName;  //商铺名称
    private String shopLocation;  //商铺精准位置
    private Integer shopApplyStatus;  //商铺申请状态
}
