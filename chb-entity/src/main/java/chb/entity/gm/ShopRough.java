package chb.entity.gm;

import lombok.*;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ShopRough implements Serializable {

    private Integer shopId;  //商铺id
    private Integer userId;  //用户id
    private String shopName;  //商铺名称
    private String shopPhone;  //商铺联系方式
    private Integer shopStatus;  //商铺状态
    private String shopLocation;  //商铺精准位置
    private Integer cityCode;  //城市编码
}
