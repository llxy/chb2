package chb.entity.gm;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Admin implements Serializable {

    private String adminName;   //管理员用户名
    private String adminPwd;    //管理员密码
    private Integer shopPm;     //商铺权限
    private Integer shopTypePm; //商铺类型权限
    private Integer foodTypePm; //食物类型权限
    private Integer flagSuper;    //是否超级管理员
    private Timestamp createTime;//创建时间
}
