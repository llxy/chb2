package chb.entity.gm;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class AdminMp implements Serializable {

    private String adminName;   //管理员用户名
    private String adminPwd;    //管理员密码
    private Integer shopMp;     //商铺权限
    private Integer shopTypeMp; //商铺类型权限
    private Integer foodTypeMp; //食物类型权限
    private Integer flagSuper;    //是否超级管理员
    private Timestamp createTime;//创建时间

    public AdminMp(Admin admin){
        this.adminName = admin.getAdminName();
        this.adminPwd = admin.getAdminPwd();
        this.shopMp = admin.getShopPm();
        this.shopTypeMp = admin.getShopTypePm();
        this.foodTypeMp = admin.getFoodTypePm();
        this.flagSuper = admin.getFlagSuper();
        this.createTime = admin.getCreateTime();
    }
}
