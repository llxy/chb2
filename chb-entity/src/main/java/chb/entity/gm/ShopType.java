package chb.entity.gm;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ShopType implements Serializable{

    private Integer shopTypeId;  //商铺类型id
    private String typeDes; //类型描述
    private Timestamp createTime;  //创建时间
}
