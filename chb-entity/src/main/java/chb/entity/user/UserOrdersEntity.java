/**
 * 
 */
package chb.entity.user;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Eric
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserOrdersEntity implements Serializable {

	/**  */
	private static final long serialVersionUID = 1L;

	private Integer userOrderId;
	private Integer userId;        //用户id
	private Integer shopId;        //商铺id
	private String shopName;       //商铺名称
	private String orderCode;      //订单编号
	private double amount;         //订单金额
	private Integer status;        //订单状态 
	private long createTime;       //订单创建时间
	private String remarks;        //订单备注
	private String deliveryWay;    //配送方式
	private String payWay;         //支付方式
	private String acceptAddress;  //收获地址
	
	private List<Dish> dishs;      //食物列表

}
