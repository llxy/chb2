/**
 * 
 */
package chb.entity.user;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author Eric
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Dish implements Serializable {

	/**  */
	private static final long serialVersionUID = 1L;

	private Integer userOrdersId;
	private Integer dishId;
	private double dishPrice;
	private Integer dishNum;
	private String dishName;
}
