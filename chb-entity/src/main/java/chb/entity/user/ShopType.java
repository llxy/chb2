/**
 * 
 */
package chb.entity.user;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Eric
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ShopType implements Serializable {
	
	/**  */
	private static final long serialVersionUID = 1L;

	private Integer shopTypeCode;
	private String shopType;
}
