/**
 * 
 */
package chb.entity.user;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;

/**
 * 用户信息实体
 * @author Eric
 */
@Data
@Builder
public class UserEntity implements Serializable {

	/**  */
	private static final long serialVersionUID = 1L;

	private Integer userId;
	private String password;
	private String phone;
	private Integer isMerchant;
	
}
