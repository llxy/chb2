/**
 * 
 */
package chb.entity.user;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 个人详细信息实体
 * @author Eric
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDetailEntity implements Serializable {
	
	/**  */
	private static final long serialVersionUID = 1L;

	private Integer userId;
	private String userName;
	/** 收货地址 */
	private String acceptAddress;
	private String avator;
	private Integer gender;
	private String phone;
	/** 个人简介 */
	private String introduction;
	private Integer shopId;
	
}
