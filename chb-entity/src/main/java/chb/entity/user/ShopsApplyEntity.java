/**
 * 
 */
package chb.entity.user;

import java.io.Serializable;

import lombok.*;

/**
 * 商铺申请表实体
 * @author Eric
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class ShopsApplyEntity implements Serializable {

	/**  */
	private static final long serialVersionUID = 1L;
	
	private Integer shopApplyId;
	private Integer userId;
	private String identificationNum;
	private String identificationPic;
	private String shopName;
	private String shopLogo;
	private String shopAbstract;
	private String shopLocation;
	private String shopAuthImages;
	private Integer shopApplyStatus;
	private Integer shopTypeCode;
	private double shopLongitude;
	private double shopLatitude;
	
}
