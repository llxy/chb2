package chb;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import chb.entity.shop.Dish;
import chb.entity.shop.DishComment;
import chb.entity.shop.Shop;
import chb.entity.shop.ShopDetail;
import chb.object.req.shop.FindShopByRangeReq;
import chb.object.resp.shop.CommentResp;
import chb.object.resp.shop.DishAndComment;
import chb.object.resp.shop.ShopAndDishResp;
import chb.object.resp.shop.ShopListResp;
import chb.object.resp.shop.ShopOrderListResp;
import chb.shop.service.IDishService;
import chb.shop.service.IShopService;
import chb.shop.service.impl.ShopOrderService;
import chb.user.mapper.IUserOrderMapper;
import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=Application.class)
@Slf4j
public class TestService {

    @Autowired
    private IShopService shopService;
    
    @Autowired
    private IDishService dishService;
    
    @Autowired
    private ShopOrderService shopOrderService;
    
    @Autowired
    private IUserOrderMapper userOrderMapper;
    
    @Test
    public void testUpdateUserOrderStatus() {
        userOrderMapper.updateStatus(1, 2);
    }
    
    @Test
    public void testAddComment() {
        
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        DishComment req = DishComment.builder()
                   .comment("好吃好吃")
                   .dishId(1)
                   .shopId(1)
                   .userId(1)
                   .level(5F)
                   .commentDate(format.format(new Date()))
                   .build();
        dishService.addComment(req);
        
        req = DishComment.builder()
                .comment("很有特色，就是有点贵呀...")
                .dishId(7)
                .shopId(1)
                .userId(1)
                .level(4.0F)
                .commentDate(format.format(new Date()))
                .build();
        dishService.addComment(req);
        
        req = DishComment.builder()
                .comment("i like it")
                .dishId(7)
                .shopId(1)
                .userId(1)
                .level(4.0F)
                .commentDate(format.format(new Date()))
                .build();
        dishService.addComment(req);
    }
    
    
    @Test
    public void testGetNewOrderNum() {
        log.info("{}", shopOrderService.getNewOrderNum(1));
    }
    
    @Test
    public void testFindComment() {
        CommentResp comment = dishService.findComment(1, 1);
        log.info("{}", comment);
        log.info("{}", new ArrayList(comment.getDishComment()));
    }
    
    @Test
    public void testHandleOrder() throws Exception {
        shopOrderService.handleOrder(1, 1, 1);
        System.in.read();
        shopOrderService.handleOrder(1, 1, 3);
        shopOrderService.handleOrder(1, 2, 2);
    }
    
    @Test
    public void testFindOrder() {
        ShopOrderListResp findOrder = shopOrderService.findOrder(1, null, 1);
        log.info("{}", findOrder);
    }
    
    @Test
    @Transactional
    @Rollback
    public void testUpdate() {
        ShopDetail shopDetail = ShopDetail.builder()
                  .shopId(1)
                  .shopName("第四食堂")
                  .shopLocation("广东省湛江市广东海洋大学主校区")
                  .shopPhone("13256")
                  .shopDeliveryCost(1.222)
                  .build();
        log.info("{}", shopDetail);
        shopService.update(shopDetail);
    }
    
    @Test
    public void testFindDishAndComment() {
        DishAndComment dishAndComment = dishService.findDishAndComment(1, 4);
        log.info("{}", dishAndComment);
    }
    
    @Test
    public void testFindDish() {
        List<Dish> dish = dishService.findDish(1, null);
        log.info("{}", dish);
        dish = dishService.findDish(1, 1);
        log.info("{}", dish);
    }
    
    @Test
    public void testFindById() {
        ShopAndDishResp resq = shopService.findById(1);
        log.info("{}", resq);
    }
    
    @Test
    public void testFindAll() {
        List<Shop> shops = shopService.findAll();
        log.info("{}", shops);
        log.info("{}", shops.size());
    }
    
    @Test
    public void testFindByRange() {
        FindShopByRangeReq req = FindShopByRangeReq.builder()
                    .latitude(20D)
                    .longitude(110D)
                    .pageNum(1)
                    .build();
        ShopListResp shops = shopService.findByRange(req);
        log.info("{}", shops);
        log.info("{}", shops.getPageNum());
        log.info("{}", shops.getTotalPage());
        log.info("{}", shops.getShopBrieflys().size());
        log.info("{}", new ArrayList(shops.getShopBrieflys()));
    }
    
    @Test
    public void testFindByRange2() {
        FindShopByRangeReq req = FindShopByRangeReq.builder()
                .latitude(20D)
                .longitude(110D)
                .name("大")
                .pageNum(1)
                .build();
        ShopListResp shops = shopService.findByRange(req);
        log.info("{}", shops);
        log.info("{}", shops.getPageNum());
        log.info("{}", shops.getTotalPage());
        log.info("{}", shops.getShopBrieflys().size());
        log.info("{}", new ArrayList(shops.getShopBrieflys()));
    }
    
}
