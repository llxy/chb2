package chb;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import chb.entity.shop.CommentCountAndLevel;
import chb.entity.shop.Dish;
import chb.entity.shop.DishBriefly;
import chb.entity.shop.DishComment;
import chb.object.req.shop.AddDishReq;
import chb.object.req.shop.ModifDishReq;
import chb.shop.mapper.IDishMapper;
import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=Application.class)
@Slf4j
public class TestDishDao {
    
    @Autowired
    private IDishMapper mapper;
    
    @Test 
    public void updateSMonthlySales() {
        mapper.updateShopMonthlySales(9, 9);
        mapper.updateDishMonthlySales(1, 1, 2);
    }
   
    @Test
    public void updateFindShopCommentCountAndLevel() {
        CommentCountAndLevel build = CommentCountAndLevel.builder()
                        .commentCount(4)
                        .level(4F)
                        .build();
        mapper.updateShopCommentCountAndLevel(build, 5);
    }
    
    @Test
    public void testFindShopCommentCountAndLevel() {
        CommentCountAndLevel countAndLevel = mapper.findShopCommentCountAndLevel(5);
        log.info("{}", countAndLevel);
    }
    
    @Test
    public void updateFindDishCommentCountAndLevel() {
        CommentCountAndLevel build = CommentCountAndLevel.builder()
                        .commentCount(4)
                        .level(4F)
                        .build();
        mapper.updateDishCommentCountAndLevel(build, 1, 1);
    }
    
    @Test
    public void testFindDishCommentCountAndLevel() {
        CommentCountAndLevel countAndLevel = mapper.findDishCommentCountAndLevel(1, 1);
        log.info("{}", countAndLevel);
    }
    
    
    @Test
    public void testAddComment() {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        DishComment req = DishComment.builder()
                   .comment("豆腐挺不错的，下次还点")
                   .dishId(1)
                   .shopId(1)
                   .userId(1)
                   .level(4.5F)
                   .commentDate(format.format(new Date()))
                   .build();
        mapper.addComment(req);
        
        req = DishComment.builder()
                .comment("很有特色，就是有点贵呀...")
                .dishId(7)
                .shopId(1)
                .userId(1)
                .level(4.0F)
                .commentDate(format.format(new Date()))
                .build();
        mapper.addComment(req);
        
        req = DishComment.builder()
                .comment("i like it")
                .dishId(7)
                .shopId(1)
                .userId(1)
                .level(4.0F)
                .commentDate(format.format(new Date()))
                .build();
        mapper.addComment(req);
    }
    
    @Test
    public void testFindCommentByShopId() {
        List<DishComment> comments = mapper.findCommentByShopId(1);
        log.info("{}", comments);
    }
    
    @Test
    public void testFindCommentByDishId() {
        List<DishComment> comments = mapper.findCommentByDishId(1, 4);
        log.info("{}", comments);
    }
    
    @Test
    public void testFindDishById() {
        DishBriefly dish = mapper.findDishById(1, 4);
        log.info("{}", dish);
    }
    
    @Test
    public void testAddDish() {
        AddDishReq req = AddDishReq.builder()
                  .shopId(3)
                  .dishName("重庆火锅")
                  .dishAbstract("重庆特色，非常好吃")
                  .dishImage("/img/shop/5360045cfc75443ea22d725133588bd4.jpg")
                  .dishType(1)
                  .dishPrice(5.2)
                  .dishType(1)
                  .build();
        mapper.addDish(req);
        
        req = AddDishReq.builder()
                .shopId(3)
                .dishName("蚝汁菠菜")
                .dishAbstract("巨好吃")
                .dishImage("/img/shop/e64af487d0c14c28a277b8dce570d68c.jpg")
                .dishType(1)
                .dishPrice(15.2)
                .dishType(1)
                .build();
        mapper.addDish(req);
        
        req = AddDishReq.builder()
                .shopId(3)
                .dishName("傣味酸辣米线")
                .dishAbstract("傣味酸辣米线")
                .dishImage("/img/shop/e7a210a8adb94170a6139292aded6cb4.jpg")
                .dishType(1)
                .dishPrice(25.2)
                .dishType(1)
                .build();
        mapper.addDish(req);
    }
    
    @Test
    public void testDelDish() {
        mapper.delDish(1, 1);
    }
    
    @Test
    public void testModifDish() {
        ModifDishReq build = ModifDishReq.builder()
                    .shopId(1)
                    .dishId(1)
                    .dishName("干锅豆腐")
                    .dishImage("333")
                    .dishAbstract("good")
                    .build();
        mapper.modifDish(build);
    }
    
    @Test
    public void testFindByShopId() {
        List<Dish> list = mapper.findDish(2, null);
        log.info("{}", list.size());
        log.info("{}", list);
    }
    
}
