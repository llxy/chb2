package chb;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import chb.entity.shop.ShopOrder;
import chb.shop.mapper.IShopOrderMapper;
import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=Application.class)
@Slf4j
public class TestShopOrderDao {
    
    @Autowired
    private IShopOrderMapper shopOrderMapper;
    
    @Test
    public void getFindUserOrderIdCount() {
        log.info("{}", shopOrderMapper.findUserOrderIdCount(1));
    }
    
    @Test
    public void testSetUserOrderId() {
        shopOrderMapper.setUserOrderId(1, 1);
    }
    
    @Test
    public void getNewOrderNum() {
        log.info("{}", shopOrderMapper.getNewOrderNum(1, 5));
    }
    
    @Test
    public void add() {
        ShopOrder order = ShopOrder.builder()
                 .shopId(1)
                 .userId(1)
                 .userNmae("cck")
                 .userPhone("123132")
                 .dishIdList("1,2")
                 .dishNumList("1,1")
                 .acceptAddress("hd")
                 .orderAmount(2.2D)
                 .orderRemarks("bu yao la")
                 .orderStatus(1)
                 .time("12")
                 .build();
        shopOrderMapper.addOrder(order);
    }
    
    @Test
    public void hand() {
        shopOrderMapper.handle(1, 1, 2);
    }
    
    @Test
    public void findShopOrder() {
        ShopOrder order = shopOrderMapper.findOrder(1, 1);
        log.info("{}", order);
    }
    
    @Test
    public void findAllShopOrder() {
        List<ShopOrder> order = shopOrderMapper.findAllShopOrder(1, null);
        log.info("{}", order);
    }
    
}
