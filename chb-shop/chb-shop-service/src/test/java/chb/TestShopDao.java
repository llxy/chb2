package chb;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import chb.entity.shop.Shop;
import chb.entity.shop.ShopBriefly;
import chb.entity.shop.ShopDetail;
import chb.shop.mapper.IShopMapper;
import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=Application.class)
@Slf4j
public class TestShopDao {
    
    @Autowired
    private IShopMapper shopMapper;
    
    @Test
    public void testFindShopDeliveryCost() {
        log.info(shopMapper.findShopDeliveryCost(5) + "");
    }
    
    @Test
    public void testFindPhone() {
        log.info(shopMapper.findPhone(5));
    }
    
    @Test
    public void testFindShopByUserId() {
        log.info(shopMapper.findShopByUserId(33) + "");
    }
    
    @Test
    public void testFindById() {
        ShopDetail detail = shopMapper.findById(1);
        log.info("{}", detail);
    }
    
    @Test
    public void testFindByRange() {
        List<ShopBriefly> list = shopMapper.findByRange(45.0, 55.0, 2, 1, null);
        log.info("{}", list);
        List<ShopBriefly> list2 = shopMapper.findByRange(45.0, 55.0, 2, null, null);
        log.info("{}", list2);
    }
    
    @Test
    public void testFindAll() {
        List<Shop> shops = shopMapper.findAll();
        Assert.assertNotNull(shops);
        log.info("{}", shops);
    }
    
    @Test
    public void testAdd() {
        Shop shop = Shop.builder()
            .userId(3)
            .shopName("饭戒") 
            .shopType(1) 
            .logo("img/shop/5360045cfc75443ea22d725133588bd4.jpg")
            .shopAbstract("shitang")     
            .shopLocation("广东省湛江市")     
            .shopAnnouncement("饭戒——一个神奇的互联网外卖！") 
            .shopStatus(1)      
            .shopPhone("15814907335")        
            .shopDeliveryCost(1.5) 
            .shopStartDelivery(4.5)
            .shopLongitude(110.307239)
            .shopLatitude(21.157362)
            .storesImages("/img/shop/5360045cfc75443ea22d725133588bd4.jpg") 
            .detailImages("/img/shop/5360045cfc75443ea22d725133588bd4.jpg") 
            .build();
        
        for(int i = 0; i < 10; i++) {
            shop.setShopName(shop.getShopName() + i);
            shopMapper.add(shop);
        }
        log.info("{}", shop.getShopId());
    }
    
    @Test
    public void testUpdate() {
        ShopDetail shopDetail = ShopDetail.builder()
                .shopId(8)
                .shopName("铭香饭店")
                .logo("img/shop/a776b759bb5a443dba24b03b2d084532.jpg")
                .shopAbstract("shitang")     
                .shopLocation("海大")     
                .shopAnnouncement("hello") 
                .shopStatus(1)      
                .shopPhone("15")        
                .shopWorkTime("17")     
                .shopDeliveryCost(1.5) 
                .shopStartDelivery(4.5)
                .storesImages("/img/shop/a776b759bb5a443dba24b03b2d084532.jpg") 
                .detailImages("/img/shop/a776b759bb5a443dba24b03b2d084532.jpg") 
                .build();
        shopMapper.update(shopDetail);
    }
    
}
