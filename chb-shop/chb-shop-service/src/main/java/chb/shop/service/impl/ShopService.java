package chb.shop.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import chb.entity.shop.FoodTypeList;
import chb.entity.shop.Shop;
import chb.entity.shop.ShopBriefly;
import chb.entity.shop.ShopDetail;
import chb.object.req.shop.FindShopByRangeReq;
import chb.object.resp.shop.ShopAndDishResp;
import chb.object.resp.shop.ShopListResp;
import chb.shop.mapper.IDishMapper;
import chb.shop.mapper.IFoodTypeMapper;
import chb.shop.mapper.IShopMapper;
import chb.shop.service.IShopService;
import lombok.extern.slf4j.Slf4j;

@Service
@Component
@Slf4j
public class ShopService implements IShopService {

    @Autowired
    private IShopMapper shopMapper;
    
    @Autowired
    private IDishMapper dishmapper;
    
    @Autowired
    IFoodTypeMapper foodTypeMapper;
    
    @Value("${pageHelper.pageSize}")
    private Integer pageSize;
    
    @Value("${shop.search.range}")
    private Integer range;
    
    @Override
    public ShopDetail findShopMsg(Integer shopId) {
        return shopMapper.findById(shopId);
    }
    
    @Override
    public ShopAndDishResp findById(Integer shopId) {
        
        return ShopAndDishResp.builder()
                    .shopDetail(shopMapper.findById(shopId))
                    .dishs(dishmapper.findDish(shopId, null))
                    .build();
    }
    
    @Override
    public ShopListResp findByRange(FindShopByRangeReq req) {
        PageHelper.startPage(req.getPageNum(), pageSize);
        PageInfo<ShopBriefly> info = new PageInfo<ShopBriefly>
            (shopMapper.findByRange(req.getLongitude(), 
                                    req.getLatitude(), 
                                    range, 
                                    req.getShopType(),
                                    req.getName()));
        
        return ShopListResp.builder()
                    .pageNum(info.getPageNum())
                    .totalPage(info.getPages())
                    .shopBrieflys(info.getList())
                    .build();
    }
    
    @Override
    public Integer add(Shop shop) {
        // 不在这使用
        shop.setUserId(33);
        shop.setShopStatus(1);
        shopMapper.add(shop);
        return null;
    }

    @Override
    public Integer update(ShopDetail shopDetail) {
        
        Assert.notNull(shopDetail.getShopId(), "商铺id为空");
        log.info("update shop {}", shopDetail);
        return shopMapper.update(ShopDetail.removeNullStr(shopDetail));
    }

    @Override
    public List<Shop> findAll() {
        return null;
    }

    @Override
    public FoodTypeList findAllFoodType() {
        return FoodTypeList
                .builder()
                .list(foodTypeMapper.findAll())
                .build();
    }

}
