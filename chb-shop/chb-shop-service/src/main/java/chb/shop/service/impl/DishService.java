package chb.shop.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import chb.entity.shop.CommentCountAndLevel;
import chb.entity.shop.Dish;
import chb.entity.shop.DishBriefly;
import chb.entity.shop.DishComment;
import chb.object.req.shop.AddDishReq;
import chb.object.req.shop.ModifDishReq;
import chb.object.resp.shop.CommentResp;
import chb.object.resp.shop.DishAndComment;
import chb.shop.mapper.IDishMapper;
import chb.shop.service.IDishService;

@Service
@Component
public class DishService implements IDishService {

    @Autowired
    private IDishMapper dishmapper;
    
    @Value("${pageHelper.commentPageSize}")
    private Integer commentPageSize;
    
    
    private static ThreadLocal<DateFormat> dateFormat = new ThreadLocal<DateFormat>() {
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd");
        }
    };
    
    @Override
    public void addComment(DishComment req) {
        req.setCommentDate(dateFormat.get().format(new Date()));
        // 看是否有评价的星星
        if(req.getLevel() == null) {
            req.setLevel(4F);
        }
        // 更新菜品平均评分
        CommentCountAndLevel countAndLevel = dishmapper
                    .findDishCommentCountAndLevel(req.getShopId(), req.getDishId());
        Integer count = countAndLevel.getCommentCount();
        Float level = countAndLevel.getLevel();
        countAndLevel.setCommentCount(count + 1);
        countAndLevel.setLevel((count * level + req.getLevel()) / (count + 1));
        dishmapper.updateDishCommentCountAndLevel(countAndLevel, req.getShopId(), req.getDishId());
        
        // 更新商铺平均评分
        countAndLevel = dishmapper
                .findShopCommentCountAndLevel(req.getShopId());
        count = countAndLevel.getCommentCount();
        level = countAndLevel.getLevel();
        countAndLevel.setCommentCount(count + 1);
        countAndLevel.setLevel((count * level + req.getLevel()) / (count + 1));
        dishmapper.updateShopCommentCountAndLevel(countAndLevel, req.getShopId());
        
        dishmapper.addComment(req);
    }
    
    @Override
    public CommentResp findComment(Integer shopId, Integer pageNum) {
        PageHelper.startPage(pageNum, commentPageSize);
        PageInfo<DishComment> info = new PageInfo<>(dishmapper.findCommentByShopId(shopId));
        
        return CommentResp.builder()
                          .pageNum(pageNum)
                          .totalPage(info.getPages())
                          .dishComment(info.getList())
                          .build();
    }
    
    @Override
    public DishAndComment findDishAndComment(Integer shopId, Integer dishId) {
        DishBriefly dishBriefly = dishmapper.findDishById(shopId, dishId);
        List<DishComment> comments = dishmapper.findCommentByDishId(shopId, dishId);
        return DishAndComment.builder()
                    .dish(dishBriefly)
                    .dishComment(comments)
                    .build();
    }
    
    @Override
    public void modifDish(ModifDishReq req) {
        dishmapper.modifDish(req);
    }

    @Override
    public List<Dish> findDish(Integer shopId, Integer dishType) {
        return dishmapper.findDish(shopId, dishType);
    }

    @Override
    public Integer addDish(AddDishReq req) {
        dishmapper.addDish(req);
        return req.getDishId();
    }

    @Override
    public Integer delDish(Integer shopId, Integer dishId) {
        return dishmapper.delDish(shopId, dishId);
    }

}
