package chb.shop.service.config;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 *  @Scheduled 指定该方法是计划任务
 *  处理月销量等  
 */
@Service
public class ScheduleTaskService {
    
    /**
     * 每个月的凌晨4点处理数据
     */
    @Scheduled(cron = "0 0 4 1 * ?")
    public void fixTimeExecution(){
        
    }
    
}
