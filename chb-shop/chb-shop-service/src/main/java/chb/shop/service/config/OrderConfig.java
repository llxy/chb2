package chb.shop.service.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Component
@ConfigurationProperties(prefix = "order")
@Getter
@Setter
@ToString
public class OrderConfig {
    
    private Integer statusAll;
    // 订单状态
    private Integer statusComplete;     // 已完成
    private Integer statusNoaccept;     // 未接订单
    private Integer statusMaking;       // 进行中
    private Integer statusCancel;       // 已取消
    private Integer statusReject;       // 已拒绝
    
    // 订单操作
    private Integer accept;             // 接单     -->  订单进入 进行中 状态
    private Integer noaccept;           // 不接单 -->  订单进入 已拒绝 状态
    private Integer cancel;             // 取消     -->  订单进入 已取消 状态
    private Integer delete;             // 删除     -->  删除订单
    
}
