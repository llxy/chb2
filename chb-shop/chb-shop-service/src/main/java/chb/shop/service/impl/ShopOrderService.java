package chb.shop.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import chb.entity.shop.DishBriefly;
import chb.entity.shop.ShopOrder;
import chb.object.resp.shop.ShopOrderDish;
import chb.object.resp.shop.ShopOrderListResp;
import chb.object.resp.shop.ShopOrderResp;
import chb.shop.mapper.IDishMapper;
import chb.shop.mapper.IShopOrderMapper;
import chb.shop.service.IShopOrderService;
import chb.shop.service.config.OrderConfig;
import chb.user.mapper.IUserOrderMapper;

@Service
@Component
public class ShopOrderService implements IShopOrderService {
    
    @Autowired
    private IShopOrderMapper shopOrderMapper;
    
    @Autowired
    private IDishMapper dishMapper;
    
    @Autowired
    private IUserOrderMapper userOrderMapper;
    
    @Autowired
    private OrderConfig orderConfig;
    
    @Value("${pageHelper.pageSize}")
    private Integer pageSize;
    
    @Override
    public ShopOrderListResp findOrder(Integer shopId, Integer orderStatus, Integer pageNum) {
        PageHelper.startPage(pageNum, pageSize);
        
        if(orderStatus == orderConfig.getStatusAll()) {
            orderStatus = null;
        }
        
        List<ShopOrder> shopOrders = shopOrderMapper.findAllShopOrder(shopId, orderStatus);
        PageInfo info = new PageInfo(shopOrders);
        
        List<ShopOrderResp> orders = new ArrayList<>();
        for(ShopOrder shopOrder : shopOrders) {
            
            String[] dishId = shopOrder.getDishIdList().split(",");
            String[] dishNum = shopOrder.getDishNumList().split(",");
            
            ShopOrderResp orderResp = ShopOrderResp.builder()
                .shopOrderId(shopOrder.getShopOrderId())
                .userNmae(shopOrder.getUserNmae())
                .userPhone(shopOrder.getUserPhone())
                .orderAmount(shopOrder.getOrderAmount())
                .acceptAddress(shopOrder.getAcceptAddress())
                .orderRemarks(shopOrder.getOrderRemarks())
                .orderStatus(shopOrder.getOrderStatus())
                .dishs(getDishs(shopId, dishId, dishNum))
                .build();
            orders.add(orderResp);
        }
        return ShopOrderListResp.builder()
                                .totalPage(info.getPages())
                                .pageNum(pageNum)
                                .shopOrders(orders)
                                .build();
    }
    
    // 有点蠢
    private List<ShopOrderDish> getDishs(Integer shopId, String[] dishId, String[] dishNum) {
        List<ShopOrderDish> shopOrderDishs = new ArrayList<>();
        
        for(int i = 0; i < dishId.length; i++) {
            DishBriefly briefly = dishMapper.findDishById(shopId, Integer.parseInt(dishId[i]));
            if(briefly != null) {
                ShopOrderDish orderDish = ShopOrderDish.builder()
                             .dishId(briefly.getDishId())
                             .dishName(briefly.getDishName())
                             .dishImage(briefly.getDishImage())
                             .dishPrice(briefly.getDishPrice())
                             .dishNum(Integer.parseInt(dishNum[i]))
                             .build();
                shopOrderDishs.add(orderDish);
            }
        }
        return shopOrderDishs;
    }

    // 订单处理
    @Override
    public void handleOrder(Integer shopId, Integer shopOrderId, Integer type) {
        System.out.println(orderConfig);
        
        // 找出订单
        ShopOrder order = shopOrderMapper.findOrder(shopId, shopOrderId);
        Assert.notNull(order);
        
        // 已完成订单，可以删除
        if(order.getOrderStatus() == orderConfig.getStatusComplete()) {
            if(type == orderConfig.getDelete()) {
                shopOrderMapper.delShopOrder(shopId, shopOrderId);
                return ;
            }
        }
        
        Integer status = null;
        
        // 订单为未接单状态，则允许接单与不接单操作
        if(order.getOrderStatus() == orderConfig.getStatusNoaccept()) {
            if(type == orderConfig.getAccept()) {
                status = orderConfig.getStatusMaking();
            } else if(type == orderConfig.getNoaccept()) {
                status = orderConfig.getStatusReject();
            }
        }
        
        // 订单为进行中状态，则允许取消订单操作
        if(order.getOrderStatus() == orderConfig.getStatusMaking()) {
            if(type == orderConfig.getCancel()) {
                status = orderConfig.getStatusCancel();
            }
        }
        
        Assert.notNull(status);
        shopOrderMapper.handle(shopId, shopOrderId, status);
        userOrderMapper.updateStatus(order.getUserOrderId(), status);
    }

    @Override
    public Integer getNewOrderNum(Integer shopId) {
        return shopOrderMapper.getNewOrderNum(shopId, orderConfig.getStatusNoaccept());
    }
    
}
