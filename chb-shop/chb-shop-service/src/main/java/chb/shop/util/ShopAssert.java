package chb.shop.util;

import chb.shop.exception.ShopException;

public class ShopAssert {
    
    public static void notNull(Object obj , Integer errorCode, String msg) {
        if(obj == null) {
            throw new ShopException(errorCode, msg);
        }
    }
    
}
