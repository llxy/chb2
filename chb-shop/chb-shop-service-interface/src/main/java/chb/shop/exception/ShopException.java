package chb.shop.exception;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ShopException 
                extends RuntimeException
                implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private Integer errorCode;
    private String msg;
}
