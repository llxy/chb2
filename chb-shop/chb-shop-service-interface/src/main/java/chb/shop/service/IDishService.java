package chb.shop.service;

import java.util.List;

import chb.entity.shop.Dish;
import chb.entity.shop.DishComment;
import chb.object.req.shop.AddDishReq;
import chb.object.req.shop.ModifDishReq;
import chb.object.resp.shop.CommentResp;
import chb.object.resp.shop.DishAndComment;

public interface IDishService {
    
    void addComment(DishComment req);
    void modifDish(ModifDishReq req);
    List<Dish> findDish(Integer shopId, Integer dishType);
    Integer addDish(AddDishReq req);
    Integer delDish(Integer shopId, Integer dishId);
    DishAndComment findDishAndComment(Integer shopId, Integer dishId);
    CommentResp findComment(Integer shopId, Integer pageNum);
    
}
