package chb.shop.service;

import java.util.List;

import chb.entity.shop.FoodTypeList;
import chb.entity.shop.Shop;
import chb.entity.shop.ShopDetail;
import chb.object.req.shop.FindShopByRangeReq;
import chb.object.resp.shop.ShopAndDishResp;
import chb.object.resp.shop.ShopListResp;


public interface IShopService {
    
    ShopDetail findShopMsg(Integer shopId);
    ShopAndDishResp findById(Integer shopId); 
    ShopListResp findByRange(FindShopByRangeReq req);
    List<Shop> findAll();
    Integer add(Shop shop);
    Integer update(ShopDetail shopDetail);
    FoodTypeList findAllFoodType();
}
