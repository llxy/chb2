package chb.shop.service;

import chb.object.resp.shop.ShopOrderListResp;

public interface IShopOrderService {
    
    Integer getNewOrderNum(Integer shopId);
    ShopOrderListResp findOrder(Integer shopId, Integer orderStatus, Integer pageNum);
    void handleOrder(Integer shopId, Integer shopOrderId, Integer type);
}
