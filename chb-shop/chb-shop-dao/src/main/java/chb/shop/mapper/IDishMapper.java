package chb.shop.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import chb.entity.shop.CommentCountAndLevel;
import chb.entity.shop.Dish;
import chb.entity.shop.DishBriefly;
import chb.entity.shop.DishComment;
import chb.object.req.shop.AddDishReq;
import chb.object.req.shop.ModifDishReq;

public interface IDishMapper {
    
    /**
     * 增加菜品
     * @param req
     * @return
     */
    Integer addDish(AddDishReq req);
    
    /**
     * 用户评论
     * @param req
     * @return
     */
    Integer addComment(DishComment req);
    
    /**
     * 删除菜品 
     * @param shopId
     * @param dishId
     * @return
     */
    Integer delDish( 
            @Param("shopId")Integer shopId,
            @Param("dishId")Integer dishId);
    
    /**
     * 修改菜品 
     * @param req
     * @return
     */
    Integer modifDish(ModifDishReq req);
    
    /**
     * 用户下单后，商铺月售额增加
     * @param shopId
     * @param increaseNum
     * @return
     */
    Integer updateShopMonthlySales(
            @Param("shopId")Integer shopId,
            @Param("increaseNum")Integer increaseNum);
    
    /**
     * 用户下订单之后，菜单月售额增加
     * @param shopId
     * @param dishId
     * @param increaseNum
     * @return
     */
    Integer updateDishMonthlySales(
            @Param("shopId")Integer shopId,
            @Param("dishId")Integer dishId,
            @Param("increaseNum")Integer increaseNum);
    
    /**
     * 用户评价后，更新菜品评价数及等级
     * @param obj
     * @param shopId
     * @param dishId
     * @return
     */
    Integer updateDishCommentCountAndLevel(
            @Param("obj")CommentCountAndLevel obj,
            @Param("shopId")Integer shopId,
            @Param("dishId")Integer dishId);
    
    /**
     * 用户评价后，更新商铺评价数及等级
     * @param obj
     * @param shopId
     * @return
     */
    Integer updateShopCommentCountAndLevel(
            @Param("obj")CommentCountAndLevel obj, 
            @Param("shopId")Integer shopId);
    
    /**
     * 查出菜品评价数及等级
     * @param shopId
     * @param dishId
     * @return
     */
    CommentCountAndLevel findDishCommentCountAndLevel(
            @Param("shopId")Integer shopId,
            @Param("dishId")Integer dishId);
    
    /**
     * 查出商铺评价数及等级
     * @param shopId
     * @return
     */
    CommentCountAndLevel findShopCommentCountAndLevel(Integer shopId);
    
    /**
     * 通过商铺id查找商铺评价
     * @param shopId
     * @return
     */
    List<DishComment> findCommentByShopId( 
            @Param("shopId")Integer shopId);
    
    /**
     * 查找某道菜的评价
     * @param shopId
     * @param dishId
     * @return
     */
    List<DishComment> findCommentByDishId( 
            @Param("shopId")Integer shopId,
            @Param("dishId")Integer dishId);
    
    /**
     * 通过菜品id查找菜品
     * @param shopId
     * @param dishId
     * @return
     */
    DishBriefly findDishById(
            @Param("shopId")Integer shopId,
            @Param("dishId")Integer dishId);
    
    /**
     * 通过菜品类型查找
     * @param shopId
     * @param dishType
     * @return
     */
    List<Dish> findDish(
            @Param("shopId")Integer shopId,
            @Param("dishType")Integer dishType);
    
    
}
