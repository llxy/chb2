package chb.shop.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import chb.entity.shop.ShopOrder;

public interface IShopOrderMapper {
    
    /**
     * 添加订单
     * @param order
     * @return
     */
    Integer addOrder(ShopOrder order);
    
    /**
     * 删除已经完成的订单
     * @param shopId
     * @param shopOrderId
     * @return
     */
    Integer delShopOrder(
            @Param("shopId")Integer shopId,
            @Param("shopOrderId")Integer shopOrderId);
    
    
    /**
     * 处理订单
     * @param shopId
     * @param shopOrderId
     * @param orderStatus
     * @return
     */
    Integer handle(
            @Param("shopId")Integer shopId, 
            @Param("shopOrderId")Integer shopOrderId, 
            @Param("orderStatus")Integer orderStatus);
    
    /**
     * 处理订单
     * @param shopId
     * @param shopOrderId
     * @param orderStatus
     * @return
     */
    Integer behandle(
            @Param("userOrderId")Integer userOrderId, 
            @Param("orderStatus")Integer orderStatus);
    
    /**
     * 为订单中的用户订单字段赋值
     * @param shopOrderId
     * @param userOrderId
     * @return
     */
    Integer setUserOrderId(
            @Param("shopOrderId")Integer shopOrderId, 
            @Param("userOrderId")Integer userOrderId);
    
    /**
     * 获取未接订单数
     * @param shopId
     * @param orderStatus
     * @return
     */
    Integer getNewOrderNum(
            @Param("shopId")Integer shopId, 
            @Param("orderStatus")Integer orderStatus);
    
    /**
     * 寻找所有订单
     * @param shopId
     * @param orderStatus
     * @return
     */
    List<ShopOrder> findAllShopOrder(
            @Param("shopId")Integer shopId, 
            @Param("orderStatus")Integer orderStatus);
    
    /**
     * 寻找订单
     * @param shopId
     * @param shopOrderId
     * @return
     */
    ShopOrder findOrder(
            @Param("shopId")Integer shopId,
            @Param("shopOrderId")Integer shopOrderId);
    
    /**
     * 查找数据库中userOrderId的数量
     * @param userOrderId
     * @return
     */
    Integer findUserOrderIdCount(
            @Param("userOrderId")Integer userOrderId);
    
}
