package chb.shop.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import chb.entity.shop.Shop;
import chb.entity.shop.ShopBriefly;
import chb.entity.shop.ShopDetail;

public interface IShopMapper {
    
    
    /**
     * 增加商铺
     * @param shop
     * @return
     */
    Integer add(Shop shop);
    
    /**
     * 修改商铺
     * @param shopDetail
     * @return
     */
    Integer update(ShopDetail shopDetail);
    
    /**
     * 查找商铺简介
     * @param shopId
     * @return
     */
    ShopDetail findById(Integer shopId);
    
    /**
     * 通过userId查找shopId
     * @param userId
     * @return
     */
    Integer findShopByUserId(Integer userId);
    
    /**
     * 查找商铺联系方式
     * @param shopId
     * @return
     */
    String findPhone(Integer shopId);
    
    /**
     * 获取商铺配送费
     * @param shopId
     * @return
     */
    Double findShopDeliveryCost(Integer shopId); 
    
    /**
     * 通过范围寻找商铺
     * @param longitude
     * @param latitude
     * @param range
     * @param shopType
     * @param name
     * @return
     */
    List<ShopBriefly> findByRange(
            @Param("longitude")Double longitude, 
            @Param("latitude")Double latitude, 
            @Param("range")Integer range,
            @Param("shopType")Integer shopType,
            @Param("name")String name);
    
    /**
     * 查找所有
     * @return
     */
    List<Shop> findAll();
    
}
