package chb.shop.mapper;

import java.util.List;

import chb.entity.gm.FoodType;

public interface IFoodTypeMapper {
    
    List<FoodType> findAll();
    
}
