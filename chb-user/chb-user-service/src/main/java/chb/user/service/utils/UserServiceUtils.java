/**
 * 
 */
package chb.user.service.utils;

import java.util.Random;

import org.springframework.stereotype.Component;

/**
 * 工具类
 * @author Eric
 */
@Component
public class UserServiceUtils {

	/** 生成6位随机验证码 */
	public String generateVerifyCode() {
		String code = "";
		int randNum = new Random().nextInt(999999);
		code += randNum;
		return code;
	}
	
}
