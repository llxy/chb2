package chb.user.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.alibaba.dubbo.config.annotation.Service;

import chb.entity.shop.Shop;
import chb.entity.user.ShopType;
import chb.entity.user.ShopsApplyEntity;
import chb.entity.user.User;
import chb.entity.user.UserDetailEntity;
import chb.entity.user.UserEntity;
import chb.object.code.ErrorCode;
import chb.object.req.user.LoginReq;
import chb.object.req.user.RegistReq;
import chb.object.req.user.ResetReq;
import chb.object.resp.user.ApplyState;
import chb.object.resp.user.Code;
import chb.user.mapper.IShopApplyMapper;
import chb.user.mapper.IShopMapper2;
import chb.user.mapper.IUserDetailMapper;
import chb.user.mapper.IUserMapper;
import chb.user.service.IUserService;
import chb.user.service.utils.SendCode;
import chb.user.service.utils.UserServiceUtils;


@Service
@Component
public class UserService implements IUserService {

	@Autowired
	UserServiceUtils userServiceUtils;
	
	@Autowired
	IUserMapper userMapper;
	
	@Autowired
	IUserDetailMapper userDetailMapper;

	@Autowired
	IShopApplyMapper shopApplyMapper;

	@Autowired
	IShopMapper2 shopMapper;
	
	/** 验证码Map */
	private Map<String, Long> codeMap = new HashMap<>();
	
	@Override
	public List<User> findAll(Shop shop) {
		return null;
	}

	@Override
	public void regist(RegistReq registReq) {
		
		UserEntity ue = UserEntity.builder()
				.password(registReq.getPassword())
				.phone(registReq.getPhone())
				.isMerchant(0)
				.build();
		userMapper.save(ue);
		userDetailMapper.save(UserDetailEntity.builder()
				.userId(ue.getUserId())
				.userName(UUID.randomUUID().toString().substring(0, 12))
				.phone(registReq.getPhone()).build());
	}

	@Override
	public UserDetailEntity login(LoginReq loginReq) {
		Integer userId = userMapper.auth(loginReq.getPhone(), loginReq.getPassword());
		Integer type = loginReq.getLoginType();
		UserDetailEntity userDetailEntity = null;
		if(userId != null) {
			//商户信息
			if(type == 1) {
				Integer shopId = shopMapper.findShopByUserId(userId);
				Assert.notNull(shopId, "非商户");
				
				userDetailEntity = userDetailMapper.findDetailByUserId(userId);
				userDetailEntity.setShopId(shopId);
				return userDetailEntity;
			}
			// 用户信息
			else if (type == 0) {
				return userDetailMapper.findDetailByUserId(userId);
			}
		}
		
		return null;
	}


	@Override
	public Integer resetPwd(ResetReq resetReq) {
		
		if(userMapper.checkPhone(resetReq.getPhone()) == null) {
			return ErrorCode.USER_NO_EXIST;
		}
		Assert.isTrue(userMapper.updatePwd(resetReq.getPassword(), resetReq.getPhone()) != 0, "更新失败！");
		return 1;
	}

	@Override
	public Code getVerifyCode(String phone) {
		
		String randomNum = userServiceUtils.generateVerifyCode();
		SendCode.sendMessage(phone, randomNum);
		long expiredTime = System.currentTimeMillis() + 1000 * 60;
		codeMap.put(randomNum, expiredTime);
		
		Code code = Code.builder().code(randomNum).expiredTime(expiredTime).build();
		return code;
	}

	@Override
	public Integer verifyCode(String code) {
		
		Long time = codeMap.get(code);
		if(time == null) {
			return ErrorCode.VERIFYCODE_ERROR;
		}
		if(time < System.currentTimeMillis()) {
			codeMap.remove(code);
			return ErrorCode.VERIFYCODE_EXPIRED;
		}
		
		return 0;
	}
	
	@Override
	public ShopsApplyEntity want2OpenShop(Integer userId) {
		return shopApplyMapper.findByUserId(userId);
	}
	
	@Override
	public Integer apply2bshopper(ShopsApplyEntity shopsApplyEntity) {
		/** 设置申请状态 */
		shopsApplyEntity.setShopApplyStatus(REVIEWING);
		/** 持久化申请表 */
		return shopApplyMapper.save(shopsApplyEntity);
	}

	@Override
	public ApplyState applyState(Integer userId) {
		
		ShopsApplyEntity shopsApplyEntity = shopApplyMapper.findByUserId(userId);
		return ApplyState.builder()
				.shopName(shopsApplyEntity.getShopName())
				.shopApplyStatus(shopsApplyEntity.getShopApplyStatus())
				.build();
	}
	
	@Override
	public List<ShopType> getShopTypes() {
		return shopApplyMapper.findShopType();
	}

}
