/**
 * 
 */
package chb.user.service;

import java.util.List;

import chb.entity.Orders;
import chb.entity.user.UserOrdersEntity;
import chb.object.req.order.PayBack;

/**
 *
 * @author Eric
 */
public interface IUserOrderService {

	static final Integer NO_PAY = 1;      //未支付
	static final Integer FINISH = 2;      //订单完成
	static final Integer NO_ACCEPT = 3;   //商户未接单
	static final Integer ACCEPT = 4;      //接单
	static final Integer CANCEL = 5;      //取消
	
	/**
	 * 查看订单服务，查看用户的所有订单,若参数orderStatus不为空，按状态查。若为空，查找用户全部订单
	 * @return
	 */
	Orders findOrder(Integer userId, Integer page, Integer orderType);
	
	/**
	 * 查看订单服务，根据用户提供的订单状态查找用户的订单
	 * @param userId
	 * @param orderStatus   订单状态
	 * @return
	 */
	List<UserOrdersEntity> findOrderByStatus(Integer userId, Integer orderStatus);
	
	/**
	 * 新增订单服务，用户在下单时，该服务用于将订单信息持久化，并返回userOrdersId
	 * @param userOrdersEntity
	 * @return
	 */
	Integer newOrder(UserOrdersEntity userOrdersEntity);
	
	/**
	 * 删除用户所有的已完成订单
	 * @param userId
	 * @return
	 */
	Integer deleteAll(Integer userId);
	
	/**
	 * 改变订单状态服务，订单的状态有:
	 * 	1【未支付】
	 * 	2【已完成】用户确认收货
	 * 	3【商家未接单】  该状态包含订单支付状态。订单需要支付之后才能被商家接单
	 * 	4【商家接单】
	 *  5【已取消】
	 * 	6【已删除】作为一个标识，不予将改状态的订单数据反馈到前台
	 * @param userOrdersId
	 * @param newStatus   新状态
	 * @param oldStatus   老状态
	 * @return
	 */
	Integer changeStatus(String userOrdersId, Integer newStatus, Integer oldStatus);
	
	Integer delete(Integer userOrdersId);
	
	/**
	 * 更新订单的状态
	 * @param userOrdersId
	 * @param status
	 * @return
	 */
	Integer findOrders(Integer userOrdersId);
	
	/**
	 * 查看订单状态
	 * @param userOrdersId
	 * @return
	 */
	Integer checkStatus(Integer userOrdersId);
	
	/**
	 * 订单是否存在
	 * @param userOrdersId
	 * @return
	 */
	boolean isExist(Integer userOrdersId);
	
	/**
	 * 支付服务
	 * @param p2_Orderreq    订单编号
	 */
	String pay(Integer p2_Orderreq);

	/**
	 * 处理支付平台的回调结果
	 * @param payBack
	 * @return
	 */
	Integer dealPay(PayBack payBack);
	
	/**
	 * 获取商铺联系方式
	 * @param shopId
	 * @return
	 */
	String getPhone(Integer shopId);
	
}
