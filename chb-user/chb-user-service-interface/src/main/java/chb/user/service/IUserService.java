package chb.user.service;

import java.util.List;

import chb.entity.shop.Shop;
import chb.entity.user.ShopType;
import chb.entity.user.ShopsApplyEntity;
import chb.entity.user.User;
import chb.entity.user.UserDetailEntity;
import chb.object.req.user.LoginReq;
import chb.object.req.user.RegistReq;
import chb.object.req.user.ResetReq;
import chb.object.resp.user.ApplyState;
import chb.object.resp.user.Code;

public interface IUserService {
    
	static final Integer REVIEWING = 0;    //申请审核中
	static final Integer REVIEWED_SUCCESS = 1;   //申请通过
	static final Integer EREVIEWED_FAIL = 2;   //申请不通过
	
    List<User> findAll(Shop shop);
    /**
     * 注册服务
     * @param registReq   注册信息对象
     * @return
     */
    void regist(RegistReq registReq);
    
    /**
     * 登录服务
     * 需要判断登录类型【LoginType】 0：用户  1：商户
     * @param loginReq   登录信息对象
     * @return
     */
    UserDetailEntity login(LoginReq loginReq);
    
    /**
     * 重置密码服务
     * @return
     */
    Integer resetPwd(ResetReq resetReq);
    
    /**
     * 获取验证码
     * @param phone
     * @return
     */
    Code getVerifyCode(String phone);
    
    /**
     * 校验验证码
     * @param code
     * @return
     */
    Integer verifyCode(String code);
    
    /**
     * 我要开店
     * @param userId
     * @return
     */
    ShopsApplyEntity want2OpenShop(Integer userId);
    
    /**
     * 申请成为商户
     * @param  shopsApplyEntity
     * @return 不为0 表示上传资料成功
     */
    Integer apply2bshopper(ShopsApplyEntity shopsApplyEntity);
    
    /**
     * 获取申请状态
     * @param userId
     * @return
     */
    ApplyState applyState(Integer userId);
    
    /**
     * 获取商铺类型集合
     * @return
     */
    List<ShopType> getShopTypes();
    
}