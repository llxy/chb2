/**
 * 
 */
package chb.user.service;

import chb.entity.user.UserDetailEntity;

/**
 *
 * @author Eric
 */
public interface IUserDetailService {

	/**
	 * 获取用户个人信息服务
	 * @param userId 用户id
	 * @return
	 */
	UserDetailEntity getUserInfo(Integer userId); 
	
	/**
	 * 修改用户信息服务
	 * @param userDetailEntity
	 * @return
	 */
	Integer modifyInfo(UserDetailEntity userDetailEntity);
}
