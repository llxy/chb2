/**
 * 
 */
package chb.user.mapper;

import chb.entity.user.UserDetailEntity;

/**
 *
 * @author Eric
 */
public interface IUserDetailMapper {

    /**
     * 个人信息
     * @param userId
     * @return
     */
    UserDetailEntity findDetailByUserId(Integer userId);

	Integer update(UserDetailEntity userDetailEntity);
	
	Integer save(UserDetailEntity userDetailEntity);
}
