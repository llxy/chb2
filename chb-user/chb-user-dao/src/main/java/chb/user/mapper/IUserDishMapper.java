/**
 * 
 */
package chb.user.mapper;

import java.util.List;

import chb.entity.user.Dish;

/**
 *
 * @author Eric
 */
public interface IUserDishMapper {

	/**
	 * 查看订单食物列表
	 * @param userOrdersId
	 * @return
	 */
    List<Dish> findByUserOrdersId(Integer userOrdersId);
    
    /**
     * 保存订单食物列表
     * @param Dish
     * @return
     */
    Integer save(Dish dish);
}
