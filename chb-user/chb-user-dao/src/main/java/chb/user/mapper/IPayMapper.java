/**
 * 
 */
package chb.user.mapper;

import chb.object.req.order.PayBack;

/**
 *
 * @author Eric
 */
public interface IPayMapper {

	/**
	 * 保存支付回调信息
	 * @param payBack
	 * @return
	 */
	Integer save(PayBack payBack);
	
	/**
	 * 查重
	 * @param userOrdersId
	 * @return
	 */
	Integer findByUserOrdersId(Integer userOrdersId);
	
}
