/**
 * 
 */
package chb.user.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import chb.entity.user.ShopType;
import chb.entity.user.ShopsApplyEntity;

/**
 *
 * @author Eric
 */
public interface IShopApplyMapper {

	/**
	 * 录入申请信息
	 * @param  shopsApplyEntity
	 * @return 申请表录入后产生的自增id
	 */
	Integer save(ShopsApplyEntity shopsApplyEntity);
	
	/**
	 * 查找所有的申请表实体
	 * @return  申请表实体列表
	 */
	List<ShopsApplyEntity> findAll();
	
	/**
	 * 根据状态查找申请表实体
	 * @return  申请表实体列表
	 */
	List<ShopsApplyEntity> findByStatus(Integer status);
	
	ShopsApplyEntity findByUserId(Integer userId);
	
	/**
	 * 删除申请表记录 
	 * @param shopApplyId
	 * @return
	 */
	void delete(Integer shopApplyId);
	
	/**
	 * 获取商铺类型集合
	 * @return
	 */
	List<ShopType> findShopType();
	
	/**
	 * 更新申请表状态
	 * @param shopApplyId
	 * @param state
	 * @return
	 */
	Integer updateApplyState(
			@Param("shopApplyId")Integer shopApplyId, 
			@Param("state")Integer state);
	
}
