/**
 * 
 */
package chb.user.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import chb.entity.user.UserOrdersEntity;

/**
 *
 * @author Eric
 */
public interface IUserOrderMapper {

	/**
	 * 新增订单
	 * @param userOrdersEntity
	 * @return
	 */
	Integer save(UserOrdersEntity userOrdersEntity);
	
	/**
	 * 统计总页数
	 * @param offset
	 * @return
	 */
	Integer total(@Param("offset")Integer offset, @Param("userId")Integer userId, @Param("status")Integer status);

	/**
	 * 查看所有用户订单
	 * @param  userId
	 * @return 
	 */
	List<UserOrdersEntity> findByUserId(@Param("userId")Integer userId, 
			@Param("start")Integer start, @Param("offset")Integer offset, @Param("orderStatus")Integer orderStatus);
	
	/**
	 * 根据状态查看用户订单
	 * @param userId
	 * @param status
	 * @return
	 */
	List<UserOrdersEntity> findByStatus(@Param("userId")Integer userId, @Param("status")Integer status);
	
	/**
	 * 查找订单
	 * @param userOrdersId
	 * @return
	 */
	UserOrdersEntity findByUserOrdersId(@Param("userOrdersId")Integer userOrdersId);
	
	/**
	 * 查找订单的状态
	 * @param userOrderId
	 * @return
	 */
	Integer checkStatus(Integer userOrdersId);
	
	/**
	 * 批量更改订单的状态 需要先验证订单的当前状态
	 * @param oldStatus
	 * @param newStatus
	 * @return
	 */
	Integer changeStatusAll(@Param("userId")Integer userId, @Param("oldStatus")Integer oldStatus, @Param("newStatus")Integer newStatus);
	
	/**
	 * 单挑更改订单的状态 需要先验证订单的当前状态
	 * @param oldStatus
	 * @param newStatus
	 * @return
	 */
	Integer changeStatusSingle(@Param("userOrdersId")String userOrdersId, @Param("oldStatus")Integer oldStatus, @Param("newStatus")Integer newStatus);

	/**
	 * 更新订单的状态
	 * @param status
	 * @return
	 */
	Integer updateStatus(@Param("userOrdersId")Integer userOrdersId, @Param("status")Integer status);

	/**
	 * 设置订单的支付方式
	 * @param payWay
	 * @param userOrderdId
	 * @return
	 */
	Integer updatePayWay(@Param("payWay")String payWay, @Param("userOrdersId")String userOrderdId);

	/**
	 * 检查订单是否存在
	 * @param userOrdersId
	 * @return
	 */
	Integer isOrderExist(Integer userOrdersId);
	
	Integer deleteByUserOrdersId(Integer userOrdersId);
}
