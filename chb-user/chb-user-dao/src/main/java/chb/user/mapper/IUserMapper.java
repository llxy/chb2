package chb.user.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import chb.entity.user.User;
import chb.entity.user.UserEntity;

public interface IUserMapper {

    List<User> findAll();
    
    /**
     * 保存用户注册信息
     * @param userEntity
     * @return
     */
    Integer save(UserEntity userEntity);

    /**
     * 登陆认证
     * @param userName
     * @param pwd
     * @return
     */
    Integer auth(@Param("phone")String phone, @Param("pwd")String pwd);
    
    /**
     * 检查手机是否注册过
     * @param phone
     * @return
     */
    Integer checkPhone(@Param("phone")String phone);
    
    /**
     * 重置密码
     * @param pwd
     * @param phone
     * @return
     */
    Integer updatePwd(@Param("pwd")String pwd, @Param("phone")String phone);
    
    /**
     * 设置为商户
     * @param userId
     * @return
     */
    Integer set2Merchant(Integer userId);
    
}
