/**
 * 
 */
package chb.user.mapper;

/**
 *
 * @author Eric
 */
public interface IShopMapper2 {

	/**
	 * 通过userId查找shopId
	 * @param userId
	 * @return
	 */
	Integer findShopByUserId(Integer userId);
	
	/**
	 * 查找商铺联系方式
	 * @param shopId
	 * @return
	 */
	String findPhone(Integer shopId);
}
