/**
 * 
 */
package chb.object.req.order;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Eric
 */
@Getter
@Setter
public class FindOrderReq implements Serializable {

	/**  */
	private static final long serialVersionUID = 1L;

	private Integer orderType;
	private Integer userId;
	private Integer page;
	
}
