/**
 * 
 */
package chb.object.req.order;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Eric
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PayBack implements Serializable{

	/**  */
	private static final long serialVersionUID = 1L;
	
	private Integer payId;
	private String p1_MerId;   //商户在易宝平台的唯一身份标识
	private String r0_Cmd;     //业务类型，固定值为 buy
	private String r1_Code;    //支付结果  1代表成功
	private String r2_TrxId;   //交易流水号
	private String r3_Amt;     //支付金额 单为元
	private String r4_Cur;     //交易币种  RMB
	private String r5_Pid;     //商品名称
	private String r6_Order;   //商户定义的订单号
	private String r7_Uid;     //会员id  非会员返回空
	private String r8_MP;      //商户扩展信息  （商户留言之类的）
	private String r9_BType;   //交易结果返回类型   1为重定向通知  2服务器点对点通知
	private String rb_BankId;
	private String ro_BankOrderId;//银行订单号
	private String rp_PayDate;    //支付成功时间
	private String rq_CardNo;
	private String ru_Trxtime;    //交易结果通知时间
	private String hmac;          //校验码
}
