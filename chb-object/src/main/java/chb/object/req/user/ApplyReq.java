/**
 * 
 */
package chb.object.req.user;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Eric
 */
@Getter
@Setter
public class ApplyReq implements Serializable {

	/**  */
	private static final long serialVersionUID = 1L;

	private Integer userId;
	private String identificationNum;
	private String shopName;
	private String shopAbstract;
	private String shopLocation;
	
	private String shopLogo;           //商铺logo
	private String identificationPic;  //身份证
    private String shopAuthImages;     //店铺图片
    
    private Integer shopTypeCode;      //商铺类型码
    
    private double shopLongitude;      //经度
    private double shopLatitude;       //纬度
}
