/**
 * 
 */
package chb.object.req.user;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Eric
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoginReq implements Serializable {

	/**  */
	private static final long serialVersionUID = 1L;

	private String phone;
	private String password;
	private Integer loginType;
	
}
