/**
 * 
 */
package chb.object.req.user.img;

import org.springframework.web.multipart.MultipartFile;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Eric
 */
@Getter
@Setter
public class ApplyImg {

	private MultipartFile shopLogo;           //商铺logo
	private MultipartFile identificationPic;  //身份证
    private MultipartFile shopAuthImages;     //店铺图片
}
