/**
 * 
 */
package chb.object.req.user;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Eric
 */
@Getter
@Setter
public class RegistReq implements Serializable {

	/**  */
	private static final long serialVersionUID = 1L;
	
	private String phone;
	private String password;
	/** 手机验证码 */
	private String checkCode;

}
