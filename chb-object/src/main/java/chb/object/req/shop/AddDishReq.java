package chb.object.req.shop;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AddDishReq implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private Integer dishId;
    private Integer shopId;
    private String dishName;
    private String dishImage;
    private Double dishPrice;
    private Integer dishType;
    private String dishAbstract;
    
}
