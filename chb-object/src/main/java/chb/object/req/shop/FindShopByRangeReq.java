package chb.object.req.shop;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FindShopByRangeReq implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private Double longitude;
    private Double latitude;
    private String name;
    private Integer pageNum;
    private Integer shopType;
    
}
