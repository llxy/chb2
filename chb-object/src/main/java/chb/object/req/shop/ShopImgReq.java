package chb.object.req.shop;

import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ShopImgReq {
    
    private MultipartFile logo;
    private MultipartFile storesImages;
    private MultipartFile detailImages;
    
}
