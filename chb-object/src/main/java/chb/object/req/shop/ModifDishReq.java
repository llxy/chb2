package chb.object.req.shop;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ModifDishReq implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private Integer shopId;
    private Integer dishId;
    private String dishName;
    private String dishImage;
    private Double dishPrice;
    private String dishAbstract;
    private Integer dishType;
    
}
