/**
 * 
 */
package chb.object.code;

/**
 *
 * @author Eric
 */
public class ErrorCode {
	
	/** 验证码错误 */
	public static final Integer VERIFYCODE_ERROR = 601;
	/** 验证码过期 */
	public static final Integer VERIFYCODE_EXPIRED = 602;
	
	/** 非商户 */
	public static final Integer NON_SHOPPER = 609;
	/** 手机已被注册 */
	public static final Integer REGISTERED_PHONE = 603;
	/** 用户已登录 */
	public static final Integer USER_IS_LOGIN = 604;
	/** 用户不存在 */
	public static final Integer USER_NO_EXIST = 605;
	/** 用户未登录 */
	public static final Integer USER_NO_LOGIN = 606;
	/** 认证失败 手机或密码错误 */
	public static final Integer AUTH_ERROR = 608;
	
	/** 订单不存在 */
	public static final Integer ORDER_NO_EXIST = 607;
}
