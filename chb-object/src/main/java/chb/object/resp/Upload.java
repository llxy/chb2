/**
 * 
 */
package chb.object.resp;

import java.io.Serializable;

import lombok.Data;

/**
 *
 * @author Eric
 */
@Data
public class Upload implements Serializable {
	/**  */
	private static final long serialVersionUID = 1L;
	
	private String imageUrl;
}
