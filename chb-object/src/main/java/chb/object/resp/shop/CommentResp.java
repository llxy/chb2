package chb.object.resp.shop;

import java.io.Serializable;
import java.util.List;

import chb.entity.shop.DishComment;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class CommentResp implements Serializable {     //查看商铺评价的返回
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    Integer totalPage;
    Integer pageNum;
    private List<DishComment> dishComment;
}
