package chb.object.resp.shop;

import java.io.Serializable;
import java.util.List;

import chb.entity.shop.DishBriefly;
import chb.entity.shop.DishComment;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class DishAndComment implements Serializable {   //dish
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private DishBriefly dish;
    private List<DishComment> dishComment;
    
}
