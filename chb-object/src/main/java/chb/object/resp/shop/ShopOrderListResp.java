package chb.object.resp.shop;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class ShopOrderListResp implements Serializable {     //shopOrder
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    Integer totalPage;
    Integer pageNum;
    List<ShopOrderResp> shopOrders;
    
}
