package chb.object.resp.shop;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class ShopOrderDish implements Serializable {  //shopOrder  
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
        
    private Integer dishId;      
    private String dishName;    
    private String dishImage;   
    private Double dishPrice;   
    private Integer dishNum;
    
}
