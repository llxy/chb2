package chb.object.resp.shop;

import java.io.Serializable;
import java.util.List;

import chb.entity.shop.Dish;
import chb.entity.shop.ShopDetail;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class ShopAndDishResp implements Serializable {   //findById
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private ShopDetail shopDetail;
    private List<Dish> dishs;
    
}
