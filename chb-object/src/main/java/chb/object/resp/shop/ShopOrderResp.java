package chb.object.resp.shop;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class ShopOrderResp implements Serializable {   //shopOrder
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private Integer shopOrderId;
    private String userNmae;
    private String userPhone;
    private Double orderAmount;
    private List<ShopOrderDish> dishs;
    private String acceptAddress;
    private String orderRemarks;
    private Integer orderStatus;
    
}
