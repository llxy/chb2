package chb.object.resp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaseResp {

    private int code;
    private Object data;

    public static BaseResp ok(Object obj) {
        return BaseResp.builder()
                .code(1)
                .data(obj)
                .build();
    }

    public static BaseResp ok() {
        return BaseResp.builder()
                .code(1)
                .build();
    }
    
    public static BaseResp ok(int num) {
        return BaseResp.builder()
                .code(1)
                .data(num)
                .build();
    }
    
    public static BaseResp ok(Integer code) {
        return BaseResp.builder()
                .code(code)
                .build();
    }
    
    public static BaseResp ok(Integer code, Integer mess) {
        return BaseResp.builder()
                .code(code)
                .data(mess)
                .build();
    }
    
    public static BaseResp error() {
        return BaseResp.builder()
                .code(0)
                .build();
    }
    
    public static BaseResp codeError(Integer rc) {
        return BaseResp.builder()
                .code(rc)
                .build();
    }

}
