/**
 * 
 */
package chb.object.resp.user;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;

/**
 *
 * @author Eric
 */
@Data
@Builder
public class Code implements Serializable {

	/**  */
	private static final long serialVersionUID = 1L;
	
	private String code;
	private long expiredTime;
}
