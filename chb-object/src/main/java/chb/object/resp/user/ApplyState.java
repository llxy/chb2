/**
 * 
 */
package chb.object.resp.user;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Eric
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ApplyState implements Serializable{

	/**  */
	private static final long serialVersionUID = 1L;

	private String shopName;
	private Integer shopApplyStatus;
}
