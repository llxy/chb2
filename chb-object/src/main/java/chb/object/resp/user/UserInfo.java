/**
 * 
 */
package chb.object.resp.user;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;

/**
 * 用户个人信息
 * @author Eric
 */
@Data
@Builder
public class UserInfo implements Serializable {
	/**  */
	private static final long serialVersionUID = 1L;

	private Integer userId;
	private Integer shopId;
	private String userName;
	private String phone;
	private String avator;
	private Integer gender;
	private String acceptAddress;
	private String introduction;
}
