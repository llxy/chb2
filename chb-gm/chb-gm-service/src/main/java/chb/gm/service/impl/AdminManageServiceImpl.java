package chb.gm.service.impl;

import chb.entity.gm.Admin;
import chb.entity.gm.AdminPermission;
import chb.gm.mapper.IAdminManageMapper;
import chb.gm.service.IAdminManageService;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Service
@Component
public class AdminManageServiceImpl implements IAdminManageService {

    @Autowired
    IAdminManageMapper mapper;

    private static final int PAGE_SIZE = 10;//每页显示条数

    @Override
    public PageInfo<Admin> findAll(Integer pageNum, String keyword) {
        if (pageNum == null) {
            pageNum = 1;
        }
        PageInfo<Admin> pageInfo = null;
        PageHelper.startPage(pageNum, PAGE_SIZE);
        if (Strings.isNullOrEmpty(keyword)) {
            pageInfo = new PageInfo(mapper.findAll());//如果关键字为空，查找全部
        } else {
            StringBuffer sb = new StringBuffer(keyword);
            sb.insert(0,"%");
            sb.append("%");
            pageInfo = new PageInfo(mapper.findByName(sb.toString()));//关键字不为空，按照关键字查找
        }
        if (pageInfo == null) {
            //TODO 查找失败，抛出异常
        }
        return pageInfo;
    }

    @Override
    public Admin login(String adminName, String adminPwd) {
        return mapper.login(adminName, adminPwd);
    }

    @Override
    public int insert(String adminName, String adminPwd) {
        return mapper.insert(adminName, adminPwd);
    }

    @Override
    public int delete(String adminName) {
        return mapper.delete(adminName);
    }

    @Override
    public int configurePermission(AdminPermission adminPermission) {
        return mapper.configurePermission(adminPermission);
    }
}
