package chb.gm.service.impl;

import chb.entity.gm.FoodType;
import chb.gm.mapper.IFoodTypeManageMapper;
import chb.gm.service.IFoodTypeManageService;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.List;

@Service
@Component
public class FoodTypeManageServiceImpl implements IFoodTypeManageService {

    @Autowired
    IFoodTypeManageMapper mapper;

    @Override
    public int insert(String typeDes) {
        if (mapper.findTypeDes(typeDes) > 0) {
            return 0;
        } else {
            return mapper.insert(typeDes);
        }
    }

    @Override
    public List<FoodType> findAll() {
        List<FoodType> list = mapper.findAll();
        if (list == null) {
            //TODO 查找失败，抛出异常
        }
        return list;
    }

    @Override
    public int deleteById(Integer foodTypeId) {
        return mapper.deleteById(foodTypeId);
    }
}
