package chb.gm.service.impl;

import chb.entity.gm.ShopApplyRough;
import chb.entity.shop.Shop;
import chb.entity.user.ShopsApplyEntity;
import chb.gm.mapper.IShopApplyManageMapper;
import chb.gm.mapper.IShopManageMapper;
import chb.gm.service.IShopApplyManageService;
import chb.gm.service.IShopManageService;
import chb.shop.service.IShopService;
import chb.user.mapper.IUserMapper;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Service
@Component
@Slf4j
public class ShopApplyManageServiceImpl implements IShopApplyManageService {

    @Autowired
    IShopApplyManageMapper shopApplyManageMapper;

    @Autowired
    IShopManageMapper shopManageMapper;

    @Autowired
    IUserMapper userMapper;

    private static final int PAGE_SIZE = 10;//每页显示条数
    private static final int NOT_EXAMINE = 0;//未审核
    private static final int EXAMINE_PASS = 1;//审核通过
    private static final int EXAMINE_NOT_PASS = 2;//审核不通过
    private static final int EXAMINE_SUCCESS = 1;//审核成功
    private static final int EXAMINE_FAIL = 0;//审核失败


    @Override
    public PageInfo<ShopApplyRough> findNotExamine(Integer pageNum) {
        if (pageNum == null) {
            pageNum = 1;
        }
        PageInfo<ShopApplyRough> pageInfo = null;
        PageHelper.startPage(pageNum, PAGE_SIZE);
        pageInfo = new PageInfo(shopApplyManageMapper.findAllByStatus(NOT_EXAMINE));
        if (pageInfo == null) {
            //TODO 查找失败，抛出异常
        }
        return pageInfo;
    }

    @Override
    public ShopsApplyEntity findById(Integer shopApplyId) {
        if (shopApplyId == null) {
            return null;
        }
        ShopsApplyEntity shopApply = shopApplyManageMapper.findById(shopApplyId);
        if (shopApply != null) {
            return shopApply;
        }
        return null;
    }

    @Override
    public int examine(Integer shopApplyId, Integer shopApplyStatus) {
        if (shopApplyId == null
                || (shopApplyStatus != EXAMINE_PASS && shopApplyStatus != EXAMINE_NOT_PASS)) {
            log.error("输入异常");//TODO 抛出输入异常
            return EXAMINE_FAIL;
        }
        ShopsApplyEntity shopApply = shopApplyManageMapper.findById(shopApplyId);
        if (shopApply == null) {
            log.error("查询的商铺申请不存在");
            return EXAMINE_FAIL;
        }
        int status = shopApply.getShopApplyStatus();
        if (status == NOT_EXAMINE) {  //确定该申请表未审核
            int examineResult = shopApplyManageMapper.examine(shopApplyId, shopApplyStatus);
            if (examineResult > 0 && shopApplyStatus == EXAMINE_PASS) {  //审核通过
                int insertResult = shopManageMapper.insert(shopApply);
                int setMerchantResult = userMapper.set2Merchant(shopApply.getUserId());
                if (insertResult > 0 && setMerchantResult > 0) {
                    return EXAMINE_SUCCESS;  //插入成功，审核完成
                }
                return EXAMINE_FAIL;  //插入失败，审核失败
            }
            if (examineResult > 0 && shopApplyStatus == EXAMINE_NOT_PASS) {  //审核不通过
                return EXAMINE_SUCCESS;
            }
        }
        return EXAMINE_FAIL;
    }
}
