package chb.gm.service.impl;

import chb.entity.gm.ShopType;
import chb.gm.mapper.IShopTypeManageMapper;
import chb.gm.service.IShopTypeManageService;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Service
@Component
public class ShopTypeManageServiceImpl implements IShopTypeManageService {

    @Autowired
    IShopTypeManageMapper mapper;

    @Override
    public int insert(String typeDes) {
        if (mapper.findTypeDes(typeDes) > 0) {
            return 0;
        } else {
            return mapper.insert(typeDes);
        }
    }

    @Override
    public List<ShopType> findAll() {
        List<ShopType> list = mapper.findAll();
        if (list == null){
            //TODO 查找失败，抛出异常
        }
        return list;
    }

    @Override
    public int deleteById(Integer shopTypeId) {
        return mapper.deleteById(shopTypeId);
    }
}
