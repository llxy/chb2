package chb.gm.service.impl;

import chb.entity.gm.ShopRough;
import chb.entity.shop.Shop;
import chb.gm.mapper.IShopManageMapper;
import chb.gm.service.IShopManageService;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Component
@Slf4j
public class ShopManageServiceImpl implements IShopManageService{

    @Autowired
    IShopManageMapper mapper;

    private static final int PAGE_SIZE = 10;//每页显示条数
    private static final String SHOP_ID = "shopId";
    private static final String USER_ID = "userId";
    private static final String SHOP_NAME = "shopName";

    @Override
    public PageInfo<ShopRough> findAll(Integer pageNum, String property, String keyword)
            throws NumberFormatException{
        if (pageNum == null){
            pageNum = 1;
        }
        PageHelper.startPage(pageNum, PAGE_SIZE);
        PageInfo<ShopRough> pageInfo = null;
        if (Strings.isNullOrEmpty(property) && Strings.isNullOrEmpty(keyword)){
            pageInfo = new PageInfo<ShopRough>(mapper.findAll());
            return pageInfo;
        }
        if (Strings.isNullOrEmpty(property)){
            keyword = fuzzy(keyword);
            pageInfo = new PageInfo<ShopRough>(mapper.findByKeyword(keyword));
            return pageInfo;
        }
        if(SHOP_ID.equals(property)){
            Integer shopId = Integer.parseInt(keyword);
            pageInfo = new PageInfo(mapper.findByShopId(shopId));
            return pageInfo;
        }
        if (USER_ID.equals(property)){
            Integer userId = Integer.parseInt(keyword);
            pageInfo = new PageInfo(mapper.findByUserId(userId));
            return pageInfo;
        }
        if (SHOP_NAME.equals(property)){
            keyword = fuzzy(keyword);
            log.info(keyword.toString());
            pageInfo = new PageInfo(mapper.findByShopName(keyword));
            return pageInfo;
        }
        if (pageInfo == null){
            //TODO 查找失败，抛出异常
        }
        return pageInfo;
    }

    @Override
    public Shop findShopDetailById(Integer shopId) {
        return mapper.findShopDetailById(shopId);
    }

    @Override
    public int freeze(Integer shopId) {
        return mapper.freeze(shopId);
    }

    @Override
    public int unfreeze(Integer shopId) {
        return mapper.unfreeze(shopId);
    }

    public String fuzzy(String keyword){
        if (Strings.isNullOrEmpty(keyword)){
            keyword = "%";
            return keyword;
        }
        StringBuffer sb = new StringBuffer(keyword);
        sb.insert(0, "%");
        sb.append("%");
        return sb.toString();
    }
}
