package chb.gm.service.impl;

import chb.Application;
import chb.entity.gm.ShopApplyRough;
import chb.entity.gm.ShopRough;
import chb.entity.user.ShopsApplyEntity;
import chb.gm.service.IShopApplyManageService;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@Slf4j
public class ShopApplyManageServiceImplTest {

    @Test
    public void findById() throws Exception {
        ShopsApplyEntity s = service.findById(1);
        log.info(s.toString());
    }

    @Autowired
    IShopApplyManageService service;
    @Test
    public void findNotExamine() throws Exception {

        PageInfo<ShopApplyRough> p = service.findNotExamine(1);
        List<ShopApplyRough> list = new ArrayList<ShopApplyRough>(p.getList());
        log.info(list.toString());
    }


    @Test
    public void examine() throws Exception {
        int i = service.examine(2, 1);
        if (i == 1){
            log.info("审核成功");
        }else {
            log.info("审核失败");
        }


    }

}