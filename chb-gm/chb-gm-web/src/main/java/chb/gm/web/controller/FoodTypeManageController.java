package chb.gm.web.controller;

import chb.entity.gm.FoodType;
import chb.entity.gm.FoodTypeList;
import chb.gm.service.IFoodTypeManageService;
import chb.object.resp.BaseResp;
import com.alibaba.dubbo.config.annotation.Reference;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api")
public class FoodTypeManageController {

    @Reference
    IFoodTypeManageService foodTypeManageService;

    private static final int ERROR_CODE = 200;

    @ResponseBody
    @GetMapping(value = "/foodType/management/all")
    public BaseResp findAll() {
        List<FoodType> list = foodTypeManageService.findAll();

        //数据结构处理
        FoodTypeList foodTypeList = new FoodTypeList(list);

        if (list != null) {
            return BaseResp.ok(foodTypeList);
        }
        return BaseResp.codeError(ERROR_CODE);
    }

    @ResponseBody
    @PostMapping(value = "/foodType/management/addition")
    public BaseResp addition(String typeDes) {
        int result = foodTypeManageService.insert(typeDes);
        if (result > 0) {
            return BaseResp.ok();
        }
        return BaseResp.codeError(ERROR_CODE);
    }

    @ResponseBody
    @PostMapping(value = "/foodType/management/deletion")
    public BaseResp deletion(Integer foodTypeId) {
        int result = foodTypeManageService.deleteById(foodTypeId);
        if (result > 0) {
            return BaseResp.ok();
        }
        return BaseResp.codeError(ERROR_CODE);
    }
}
