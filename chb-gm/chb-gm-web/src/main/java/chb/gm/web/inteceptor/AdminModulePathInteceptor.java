package chb.gm.web.inteceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Component
public class AdminModulePathInteceptor implements HandlerInterceptor {

    private static final int NOT_FOUND = 404;

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        StringBuffer url = httpServletRequest.getRequestURL();
        log.info("url:{}",url.toString());
        log.info("ContextPath:{}",httpServletRequest.getContextPath());
        log.info("RemoteAddr:{}",httpServletRequest.getRemoteAddr());
        log.info("PathInfo:{}",httpServletRequest.getPathInfo());
        log.info("ServletPath:{}",httpServletRequest.getServletPath());
        log.info("ServerName:{}",httpServletRequest.getServerName());
        log.info("ServerPort:{}",httpServletRequest.getServerPort());
//        StringBuffer index = new StringBuffer();
//        index.append(httpServletRequest.getScheme());
//        index.append("://");
//        index.append(httpServletRequest.getServerName());
//        index.append(":");
//        index.append(httpServletRequest.getServerPort());
//        index.append("/");
//        httpServletResponse.sendRedirect(index.toString());
/*        httpServletRequest.getRequestURL();
        httpServletRequest.getSession().setAttribute("s","123");
        httpServletRequest.getRequestDispatcher("/api/admin/fresh").forward(httpServletRequest,httpServletResponse);*/
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
//        int status = httpServletResponse.getStatus();
//        if (status == NOT_FOUND){
//            modelAndView.setView();
//
//        }
    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {


    }
}
