package chb.gm.web.inteceptor;

import chb.entity.gm.Admin;
import chb.entity.gm.AdminMp;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Slf4j
@Component
public class ShopTypeManageInterceptor implements HandlerInterceptor {

    private static final int HAVE_PM = 1;

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        HttpSession session = httpServletRequest.getSession();
        AdminMp admin = (AdminMp) session.getAttribute("loginAdmin");
        if (admin != null) {
            Integer flagSuper = admin.getFlagSuper();
            if (flagSuper == HAVE_PM) {
                log.info("是超级管理员可以管理食物类型");
                return true;
            }
            Integer shopTypePm = admin.getShopTypeMp();
            if (shopTypePm == HAVE_PM) {
                log.info("有商铺类型管理权限");
                return true;
            }
        }
        log.info("没有商铺类型管理权限");
        return false;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
