package chb.gm.web.controller;

import chb.entity.gm.ShopApplyRough;
import chb.entity.user.ShopsApplyEntity;
import chb.gm.service.IShopApplyManageService;
import chb.object.resp.BaseResp;
import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/api")
public class ShopApplyManageController {

    @Reference
    IShopApplyManageService shopApplyManageService;

    private static final int ERROR_CODE = 200;

    @ResponseBody
    @GetMapping(value = "/shopApply/management/all")
    public BaseResp findAll(Integer pageNum) {
        PageInfo<ShopApplyRough> pageInfo = shopApplyManageService.findNotExamine(pageNum);
        if (pageInfo != null) {
            return BaseResp.ok(pageInfo);
        }
        return BaseResp.codeError(ERROR_CODE);
    }

    @ResponseBody
    @GetMapping(value = "/shopApply/management/{shopApplyId}")
    public BaseResp findById(@PathVariable(value = "shopApplyId") Integer shopApplyId) {
        ShopsApplyEntity shopApply = shopApplyManageService.findById(shopApplyId);
        if (shopApply != null){
            return BaseResp.ok(shopApply);
        }
        return BaseResp.codeError(ERROR_CODE);
    }

    @ResponseBody
    @PostMapping(value = "/shopApply/management/examination")
    public BaseResp examination(Integer shopApplyId, Integer result) {
        int examineResult = shopApplyManageService.examine(shopApplyId, result);
        if (examineResult == 1){
            return BaseResp.ok();
        }
        return BaseResp.codeError(ERROR_CODE);
    }

}
