package chb.gm.web.controller;

import chb.entity.gm.Admin;
import chb.entity.gm.AdminMp;
import chb.entity.gm.AdminPermission;
import chb.gm.service.IAdminManageService;
import chb.object.resp.BaseResp;
import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;


@RestController
@Slf4j
@RequestMapping("/api")
public class AdminManageController {

    @Reference
    IAdminManageService adminManageService;

    @Autowired
    HttpSession session;

    private static final int ERROR_CODE = 200;

    @ResponseBody
    @GetMapping(value = "/admin/all")
    public BaseResp findAll(Integer pageNum, String keyword) {
        PageInfo<Admin> pageInfo = adminManageService.findAll(pageNum, keyword);  //分页数据集

        //数据结构处理
        List<Admin> list = pageInfo.getList();
        List<AdminMp> listMp = new ArrayList<AdminMp>();
        for (Admin admin: list) {
            AdminMp adminMp = new AdminMp(admin);
            listMp.add(adminMp);
        }
        PageInfo<AdminMp> pageInfoMp = new PageInfo(listMp);

        if (pageInfo != null) {
            return BaseResp.ok(pageInfoMp);  //查找成功
        }
        return BaseResp.codeError(ERROR_CODE);//查找失败
    }

    @ResponseBody
    @PostMapping(value = "/admin/login")
    public BaseResp login(String adminName, String adminPwd) {
        Admin admin = adminManageService.login(adminName, adminPwd);

        // 数据结构处理
        AdminMp adminMp = new AdminMp(admin);


        if (admin != null) {
            session.setAttribute("loginAdmin", adminMp);
            return BaseResp.ok(adminMp);
        }
        return BaseResp.codeError(ERROR_CODE);
    }

    @ResponseBody
    @PostMapping(value = "/admin/configuration")
    public BaseResp configuration(String adminName,Integer shopMp,Integer shopTypeMp,Integer foodTypeMp) {
        AdminPermission adminPm = new AdminPermission(adminName,shopMp,shopTypeMp,foodTypeMp);
        int result = adminManageService.configurePermission(adminPm);
        if (result > 0) {
            return BaseResp.ok();
        }
        return BaseResp.codeError(ERROR_CODE);
    }

    @ResponseBody
    @PostMapping(value = "/admin/addition")
    public BaseResp addition(String adminName, String adminPwd) {
        int result = adminManageService.insert(adminName, adminPwd);
        if (result > 0){
            return BaseResp.ok();
        }
        return BaseResp.codeError(ERROR_CODE);
    }

    @ResponseBody
    @PostMapping(value = "/admin/deletion")
    public BaseResp deletion(String adminName){
        int result = adminManageService.delete(adminName);
        if (result > 0){
            return BaseResp.ok();
        }
        return BaseResp.codeError(ERROR_CODE);
    }

    @ResponseBody
    @PostMapping(value = "/admin/logout")
    public BaseResp logout(){
        try {
            session.removeAttribute("loginAdmin");
            return BaseResp.ok();
        } catch (Exception e) {
            log.error(e.getMessage());
            return BaseResp.codeError(ERROR_CODE);
        }
    }
}
