package chb.gm.web.controller;

import chb.entity.gm.ShopType;
import chb.entity.gm.ShopTypeList;
import chb.gm.service.IShopTypeManageService;
import chb.object.resp.BaseResp;
import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api")
public class ShopTypeManageController {

    @Reference
    IShopTypeManageService shopTypeManageService;

    private static final int ERROR_CODE = 200;

    @ResponseBody
    @GetMapping(value = "/shopType/management/all")
    public BaseResp findAll() {
        List<ShopType> list = shopTypeManageService.findAll();

        //数据结构处理
        ShopTypeList shopTypeList = new ShopTypeList(list);

        if (list != null) {
            return BaseResp.ok(shopTypeList);
        }
        return BaseResp.codeError(ERROR_CODE);
    }

    @ResponseBody
    @PostMapping(value = "/shopType/management/addition")
    public BaseResp addition(String typeDes) {
        int result = shopTypeManageService.insert(typeDes);
        if (result > 0) {
            return BaseResp.ok();
        }
        return BaseResp.codeError(ERROR_CODE);
    }

    @ResponseBody
    @PostMapping(value = "/shopType/management/deletion")
    public BaseResp deletion(Integer shopTypeId) {
        int result = shopTypeManageService.deleteById(shopTypeId);
        if (result > 0) {
            return BaseResp.ok();
        }
        return BaseResp.codeError(ERROR_CODE);
    }
}
