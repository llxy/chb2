package chb.gm.web.controller;

import chb.entity.gm.ShopRough;
import chb.entity.shop.Shop;
import chb.gm.service.IShopApplyManageService;
import chb.gm.service.IShopManageService;
import chb.object.resp.BaseResp;
import com.alibaba.dubbo.config.annotation.Reference;
import com.fasterxml.jackson.databind.ser.Serializers;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/api")
public class ShopManageController {

    @Reference
    IShopManageService shopManageService;


    private static final int ERROR_CODE = 200;
    private static final String TYPE_FREEZE = "freeze";
    private static final String TYPE_UNFREEZE = "unfreeze";

    @ResponseBody
    @GetMapping(value = "/shop/management/all")
    public BaseResp findAll(Integer pageNum, String property, String keyword) {
        PageInfo<ShopRough> pageInfo = shopManageService.findAll(pageNum, property, keyword);
        if (pageInfo != null) {
            return BaseResp.ok(pageInfo);
        }
        return BaseResp.codeError(ERROR_CODE);
    }

    @ResponseBody
    @GetMapping(value = "/shop/management/{shopId}")
    public BaseResp findById(@PathVariable("shopId") Integer shopId) {
        Shop shop = shopManageService.findShopDetailById(shopId);
        if (shop != null) {
            return BaseResp.ok(shop);
        }
        return BaseResp.codeError(ERROR_CODE);
    }

    @ResponseBody
        @PostMapping(value = "/shop/management/freeze")
        public BaseResp freeze(Integer shopId, String type) {
            int result = 0;
            if (TYPE_FREEZE.equals(type)) {
                result = shopManageService.freeze(shopId);
            }
            if (TYPE_UNFREEZE.equals(type)) {
                result = shopManageService.unfreeze(shopId);
            }
        if (result > 0){
            return BaseResp.ok();
        }
        return BaseResp.codeError(ERROR_CODE);
    }

}
