package chb.gm.web.config;

import chb.gm.web.inteceptor.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class GMInterceptorsConfig extends WebMvcConfigurerAdapter {

    @Autowired
    AdminManageInterceptor adminManageInterceptor;
    @Autowired
    ShopManageInterceptor shopManageInterceptor;
    @Autowired
    ShopTypeManageInterceptor shopTypeManageInterceptor;
    @Autowired
    FoodTypeManageInteceptor foodTypeManageInteceptor;
    @Autowired
    AdminModulePathInteceptor adminModulePathInteceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(adminManageInterceptor)
                .addPathPatterns("/api/admin/*")
                .excludePathPatterns("/api/admin/login");
        registry.addInterceptor(shopManageInterceptor)
                .addPathPatterns("/api/shop/managemnt/*")
                .addPathPatterns("/api/shopApply/managemnt/*");
        registry.addInterceptor(shopTypeManageInterceptor)
                .addPathPatterns("/api/shopType/management/*");
        registry.addInterceptor(foodTypeManageInteceptor)
                .addPathPatterns("/api/foodType/management/*");
    }
}
