package chb.gm.service;

import chb.entity.gm.Admin;
import chb.entity.gm.AdminPermission;
import com.github.pagehelper.PageInfo;


public interface IAdminManageService {

    PageInfo<Admin> findAll(Integer pageNum, String keyword);

    Admin login(String adminName, String adminPwd);

    int insert(String adminName, String adminPwd);

    int delete(String adminName);

    int configurePermission(AdminPermission adminPermission);

}
