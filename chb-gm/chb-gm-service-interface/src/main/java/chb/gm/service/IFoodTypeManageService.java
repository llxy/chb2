package chb.gm.service;

import chb.entity.gm.FoodType;

import java.util.List;

public interface IFoodTypeManageService {

    int insert(String typeDes);  //插入食物类型

    List<FoodType> findAll();  //查询全部食物类型

    int deleteById(Integer foodTypeId);  //通过id删除食物类型
}
