package chb.gm.service;

import chb.entity.gm.ShopApplyRough;
import chb.entity.user.ShopsApplyEntity;
import com.github.pagehelper.PageInfo;

public interface IShopApplyManageService {

    PageInfo<ShopApplyRough> findNotExamine(Integer pageNum);

    ShopsApplyEntity findById(Integer shopApplyId);

    int examine(Integer shopApplyId, Integer shopApplyStatus);
}
