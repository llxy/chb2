package chb.gm.service;

import chb.entity.gm.ShopType;

import java.util.List;

public interface IShopTypeManageService {

    int insert(String typeDes);  //插入商铺类型

    List<ShopType> findAll();  //查询全部商铺类型

    int deleteById(Integer shopTypeId);  //通过id删除商铺类型
}
