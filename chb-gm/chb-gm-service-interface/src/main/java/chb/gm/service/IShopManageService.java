package chb.gm.service;

import chb.entity.gm.ShopRough;
import chb.entity.shop.Shop;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IShopManageService {

    PageInfo<ShopRough> findAll(Integer pageNum,String property,String keyword);

    Shop findShopDetailById(Integer shopId);

    int freeze(Integer shopId);

    int unfreeze(Integer shopId);

}
