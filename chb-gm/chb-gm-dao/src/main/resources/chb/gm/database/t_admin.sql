/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50711
Source Host           : localhost:3306
Source Database       : db_chb

Target Server Type    : MYSQL
Target Server Version : 50711
File Encoding         : 65001

Date: 2017-12-07 09:11:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_admin
-- ----------------------------
DROP TABLE IF EXISTS `t_admin`;
CREATE TABLE `t_admin` (
  `admin_name` varchar(30) NOT NULL COMMENT '管理员用户名',
  `admin_pwd` varchar(255) NOT NULL DEFAULT '' COMMENT '管理员密码',
  `shop_pm` tinyint(1) NOT NULL DEFAULT '0' COMMENT '商铺权限',
  `shop_type_pm` tinyint(1) NOT NULL DEFAULT '0' COMMENT '商铺类型权限',
  `food_type_pm` tinyint(1) NOT NULL DEFAULT '0' COMMENT '食物类型权限',
  `flag_super` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否超级管理员',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`admin_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
