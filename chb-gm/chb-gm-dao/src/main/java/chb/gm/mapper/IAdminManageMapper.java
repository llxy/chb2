package chb.gm.mapper;

import chb.entity.gm.Admin;
import chb.entity.gm.AdminPermission;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Mapper
public interface IAdminManageMapper {

    List<Admin> findAll();  //查询所部管理员

    List<Admin> findByName(String adminName);  //通过名字查询管理员

    int insert(@Param("adminName") String adminName,
               @Param("adminPwd") String adminPwd);  //插入管理员

    Admin login(@Param("adminName") String adminName,
                @Param("adminPwd") String adminPwd);  //查询登录的用户密码是否存在

    int delete(String adminName);  //通过管理员名字删除管理员

    int configurePermission(AdminPermission adminPermission);//配置管理员权限
}

