package chb.gm.mapper;

import chb.entity.gm.ShopApplyRough;
import chb.entity.user.ShopsApplyEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface IShopApplyManageMapper {

    List<ShopApplyRough> findAll();

    List<ShopApplyRough> findAllByStatus(Integer shopApplyStatus);

    ShopsApplyEntity findById(Integer shopApplyId);

    int examine(@Param("shopApplyId") Integer shopApplyId,
                @Param("shopApplyStatus") Integer shopApplyStatus);
}
