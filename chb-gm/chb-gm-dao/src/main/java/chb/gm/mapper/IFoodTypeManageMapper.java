package chb.gm.mapper;

import chb.entity.gm.FoodType;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface IFoodTypeManageMapper {

    int insert(String typeDes);

    int findTypeDes(String typeDes);

    List<FoodType> findAll();

    int deleteById(Integer foodTypeId);
}
