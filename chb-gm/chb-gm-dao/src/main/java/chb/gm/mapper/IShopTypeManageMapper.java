package chb.gm.mapper;

import chb.entity.gm.ShopType;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface IShopTypeManageMapper {

    int insert(String typeDes);

    int findTypeDes(String typeDes);

    List<ShopType> findAll();

    int deleteById(Integer shopTypeId);
}
