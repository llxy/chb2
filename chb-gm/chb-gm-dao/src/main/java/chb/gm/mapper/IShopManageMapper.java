package chb.gm.mapper;

import chb.entity.gm.ShopRough;
import chb.entity.shop.Shop;
import chb.entity.user.ShopsApplyEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface IShopManageMapper {

        List<ShopRough> findAll();

        List<ShopRough> findByKeyword(String keyword);

        List<ShopRough> findByShopId(Integer shopId);

        List<ShopRough> findByUserId(Integer userId);

        List<ShopRough> findByShopName(String shopName);

        Shop findShopDetailById(Integer shopId);

        int insert(ShopsApplyEntity shopApply);

        int freeze(Integer shopId);

        int unfreeze(Integer shopId);
}
