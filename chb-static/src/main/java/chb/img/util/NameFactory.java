package chb.img.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.UUID;

import org.springframework.stereotype.Component;

@Component
public class NameFactory {

    public final static ThreadLocal<DateFormat> TL_FORMAT = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        }
    };
    
    public String getName() {
        StringBuffer name = new StringBuffer();
        for(String str : UUID.randomUUID().toString().split("-")) {
            name.append(str);
        }
        return name.toString();
    }
    
}
