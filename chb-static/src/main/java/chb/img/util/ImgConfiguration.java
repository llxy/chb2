package chb.img.util;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
@ConfigurationProperties("img-configuration")
@Getter
@Setter
public class ImgConfiguration {
    
    @NotNull
    private List<String> acceptType;
    
    @NotNull
    private String shopPath;
    
    @NotNull
    private String userPath;
    
    @NotNull
    private String gmPath;
    
}
